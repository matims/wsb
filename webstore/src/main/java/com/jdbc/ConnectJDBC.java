package com.jdbc;


import com.packt.calc.domain.BankOffer;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.jdbc.ConnectionData.URL;
import static com.jdbc.Statements.INSERT_BANK_OFFER;

public class ConnectJDBC {

    private Connection connection;

    // the mysql insert statement
    public void insertBankOffersToDatabase(List<BankOffer> bankOffers) throws Exception {
        long start = System.currentTimeMillis();
        System.out.println("connect" + (start) / 1000d);
        connect();
        System.out.println("connect" + (System.currentTimeMillis() - start) / 1000d);

        connection.setAutoCommit(true);
        PrepareDataToInsert insertData = new PrepareDataToInsert(bankOffers);
        System.out.println("select" + (System.currentTimeMillis() - start) / 1000d);
        if (insertData.notSave) {
            return;
        }
        try {
            PreparedStatement preparedStmt = connection.prepareStatement(INSERT_BANK_OFFER);
            for (BankOffer bankOffer : bankOffers) {
                // create the mysql insert preparedstatement

                preparedStmt.setDate(2, Date.valueOf(LocalDate.now()));
                preparedStmt.setInt(1, ++insertData.maxId);
                preparedStmt.setString(3, bankOffer.getBankName());
                preparedStmt.setString(4, bankOffer.getTitleOfOffer());
                preparedStmt.setString(5, bankOffer.getChosenCreditAmount());
                preparedStmt.setString(6, bankOffer.getChosenPeriod());
                preparedStmt.setString(7, bankOffer.getRRSO());
                preparedStmt.setString(8, bankOffer.getCreditAmount());
                preparedStmt.setString(9, bankOffer.getInstallmentAmount());
                preparedStmt.addBatch();
                System.out.println("insert" + insertData.maxId + " " + (System.currentTimeMillis() - start) / 1000d);
            }

            preparedStmt.executeBatch();

            System.out.println("last insert" + (System.currentTimeMillis() - start) / 1000d);
            preparedStmt.close();
            closeConnection();
            System.out.println("close connect" + (System.currentTimeMillis() - start) / 1000d);
        } catch (SQLException ex) {
            System.err.println("SQLException information");
            while (ex != null) {
                System.err.println("Error msg: " + ex.getMessage());
                ex = ex.getNextException();
            }
            throw new RuntimeException("Error");
        }

    }

    private ResultSet select() throws SQLException {
        // Create and execute a SELECT SQL statement.
        Statement statement = connection.createStatement();
        return statement.executeQuery(new Statements().getSelectBankOffer());

    }

    private class PrepareDataToInsert {
        List<BankOffer> bankOffersFromDatabase = new ArrayList();
        int maxId;
        boolean notSave;

        PrepareDataToInsert(List<BankOffer> currentList) throws SQLException {
            this.bankOffersFromDatabase = getBankOffersFromDatabase();
            this.maxId = this.bankOffersFromDatabase.size() > 0 ? getMaxId() : 0;
            this.notSave = notSave(currentList);

        }


        private boolean notSave(List<BankOffer> currentList) {
            List<String> check = currentList.stream().
                    map(s -> Date.valueOf(LocalDate.now()).toString() + s.getChosenPeriod() + s.getChosenCreditAmount())
                    .distinct().collect(Collectors.toList());

            List<String> dataFromdateBase = bankOffersFromDatabase.stream().map(s -> s.getCheckDate().toString() + s.getChosenPeriod() + s.getChosenCreditAmount())
                    .distinct().collect(Collectors.toList());

            return check.stream().filter(dataFromdateBase::contains).count() > 0;
        }


        private int getMaxId() {
            return bankOffersFromDatabase.stream().max(Comparator.comparing(BankOffer::getId)).get().getId();
        }

        private List<BankOffer> getBankOffersFromDatabase() throws SQLException {
            ResultSet data = select();

            while (data.next()) {
                BankOffer b = new BankOffer();
                b.setId(data.getInt(1));
                b.setCheckDate(data.getDate(2));
                b.setBankName(data.getString(3));
                b.setTitleOfOffer(data.getString(4));
                b.setChosenCreditAmount(data.getString(5));
                b.setChosenPeriod(data.getString(6));
                b.setRRSO(data.getString(7));
                b.setCreditAmount(data.getString(8));
                b.setInstallmentAmount(data.getString(9));
                bankOffersFromDatabase.add(b);
            }
            data.close();
            return bankOffersFromDatabase;
        }
    }


    private void closeConnection() throws SQLException {
        connection.close();
    }

    private void connect() throws SQLException {
        // Connect to database
        connection = DriverManager.getConnection(URL);


    }
}



