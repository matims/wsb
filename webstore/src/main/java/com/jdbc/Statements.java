package com.jdbc;

import java.time.LocalDate;

import static java.time.format.DateTimeFormatter.ISO_DATE;

public class Statements {

    private LocalDate date = LocalDate.now();

    public String CURRENT_DATE = date.format(ISO_DATE);

    public static String INSERT_BANK_OFFER =
            " INSERT INTO [dbo].[BankOffer]"
                    + "([BankOfferIdId]"
                    + ",[CheckDate]"
                    + ",[BankName]"
                    + ",[TitleOfOffer]"
                    + ",[ChosenCreditAmount]"
                    + ",[ChosenPeriod]"
                    + ",[RRSO]"
                    + ",[CreditAmount]"
                    + ",[InstallmentAmount])"
                    + "VALUES"
                    + "(?"
                    + ",?"
                    + ",?"
                    + ",?"
                    + ",?"
                    + ",?"
                    + ",?"
                    + ",?"
                    + ",?)";

    public static String SELECT_BANK_OFFER = "SELECT [BankOfferIdId]"
            + ",[CheckDate]"
            + ",[BankName]"
            + " ,[TitleOfOffer]"
            + " ,[ChosenCreditAmount]"
            + ",[ChosenPeriod]"
            + ",[RRSO]"
            + ",[CreditAmount]"
            + ",[InstallmentAmount]"
            + "FROM [dbo].[BankOffer]";

    public String getSelectBankOffer() {
        return SELECT_BANK_OFFER;
    }
}
