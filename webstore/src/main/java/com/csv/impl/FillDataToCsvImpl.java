package com.csv.impl;

import com.packt.calc.domain.BankOffer;
import com.csv.CsvUtils;
import com.csv.FillDataToCsv;

import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FillDataToCsvImpl extends FillDataToCsv {
    int i = 0;

    @Override
    public void fillCsv(List<BankOffer> bankOfferList) {
        try {

            FileWriter writer = new FileWriter(FILE_NAME);
            for (BankOffer b : bankOfferList.stream().filter(RRSO_FILTER).collect(Collectors.toList())) {
                CsvUtils.writeLine(writer, prepareBankOffer(b));
            }
            writer.flush();
            writer.close();

        } catch (Exception e) {
            System.out.println("Błąd wykonania pliku CSV");
        }


    }

    @Override
    protected List<String> prepareBankOffer(BankOffer b) {
        List<String> list = new ArrayList<>();
        b.setId(i++);
        list.add(b.getId() + "");
        list.add(b.getBankName());
        list.add(b.getTitleOfOffer());
        list.add(b.getChosenCreditAmount());
        list.add(b.getChosenPeriod());
        list.add(b.getRRSO());
        list.add(b.getCreditAmount());
        list.add(b.getInstallmentAmount());
        return list;
    }
}
