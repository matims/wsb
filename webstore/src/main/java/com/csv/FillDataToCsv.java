package com.csv;

import com.packt.calc.domain.BankOffer;

import java.util.List;
import java.util.function.Predicate;

public abstract class FillDataToCsv {

    protected String FILE_NAME = "plik.xlsx";

    abstract public void fillCsv(List<BankOffer> bankOfferList);

    abstract protected List<String> prepareBankOffer(BankOffer b);

    protected static Predicate<BankOffer> RRSO_FILTER = b -> b.getRRSO() != null || b.getRRSO() != "";
}
