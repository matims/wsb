package com.packt.dict;

public class EmploymentType {

    public static final String EMPLOYMENT_CONTRACT = "Umowa o prace";
    public static final String RENT = "Umowa najmu";
    public static final String SELF_EMPLOYED = "Osoba fizyczna prowadzaca dzialalnosc gospodarcza";
    public static final String CONTRACT_WORK = "Umowa o dzielo";
    public static final String CIVIL_CONTACT = "Umowa o zlecenie";

}
