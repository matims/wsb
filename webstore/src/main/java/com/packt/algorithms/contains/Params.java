package com.packt.algorithms.contains;

public
class Params {
    public double MaxSocialContBase = 142950.0;
    public double RetCont = 0.0976;
    public double PenCont = 0.015;
    public double DiseaseInsCont = 0.0245;
    public double HealthInsCont = 0.09;
    public double Tax = 1335.0;
    public double tax_1 = 8000.0;
    public double tax_2 = 13000.0;
    public double tax_3 = 85528.0;
    public double tax_4 = 127000.0;
    public double taxRateLevel1 = 0.18;
    public double taxRateLevel2 = 0.32;
    public double taxAmount1 = 1440;
    public double taxAmount2 = 883.98;
    public double taxAmount3 = 556.02;
    public double taxDed2 = 5000.0;
    //double taxDed2 = 41472.0;
    public double taxDed4 = 41472.0;
    public double HealthInsCont2 = 0.0775;


}