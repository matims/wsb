package com.packt.algorithms.entities;

public class Installment {

    Double netIncome;
    Double dti;
    Double socialMinimum;

    public Double getNetIncome() {
        return netIncome;
    }

    public Double getSocialMinimum() {
        return socialMinimum;
    }

    public Double getDti() {
        return dti;
    }

    public void setDti(Double dti) {
        this.dti = dti;
    }

    public void setNetIncome(Double netIncome) {
        this.netIncome = netIncome;
    }

    public void setSocialMinimum(Double socialMinimum) {
        this.socialMinimum = socialMinimum;
    }
}
