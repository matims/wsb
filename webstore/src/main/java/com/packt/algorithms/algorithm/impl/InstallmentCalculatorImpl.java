package com.packt.algorithms.algorithm.impl;

import com.packt.algorithms.algorithm.InstallmentCalculator;
import com.packt.algorithms.entities.Installment;

public class InstallmentCalculatorImpl implements InstallmentCalculator {

    @Override
    public Double calculate(Installment installment) {
        return installment.getNetIncome() * installment.getDti();
    }

}
