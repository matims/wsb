package com.packt.algorithms.algorithm.impl;

import com.packt.calc.domain.Income;

public class IncomeResponse extends CalculationNetIncomeImpl {

    public Income prepareIncomeResponse(Income income) {
        income.setNetIncome((double) (Math.round(calculateNetIncomeForEmployeeContract(income)) * 100) / 100d);
        return income;
    }
}
