package com.packt.algorithms.algorithm.impl;


import com.packt.algorithms.algorithm.Calculation;
import com.packt.algorithms.algorithm.Data;
import com.packt.algorithms.contains.Params;
import com.packt.calc.domain.Income;

public class CalculationNetIncomeImpl extends Calculation {

    static double NettoCalc(String employmentType, double x, int a, int b, int c) {

        double wynik;

        switch (employmentType) {
            case "RZA_UPR":
                wynik = Uprac(x);
                break;
            case "RZA_ZLEC_20":
                wynik = Uzlec(x, a, b, c, 20);
                break;
            case "RZA_ZLEC_50":
                wynik = Uzlec(x, a, b, c, 50);
                break;
            case "RZA_UDZ_20":
                wynik = Uzlec(x, 0, 0, 0, 20);
                break;
            case "RZA_UDZ_50":
                wynik = Uzlec(x, 0, 0, 0, 50);
                break;
            //case "RZA_UDZ" : wynik =Udz(x,0,0,0); break;
            default:
                wynik = -999;


        }

        return wynik;

    }

    private static double min(double x, double y) {
        return (x <= y) ? x : y;
    }

    private static double max(double x, double y) {
        return (x >= y) ? x : y;
    }

    private static double Uprac(double x) {

        double wynikUPRAC;
        double SkladkiUS;
        double skladkiUZ;
        double zaliczkaPD;
        double podatek;
        double podstawa;
        double zmn;

        Params p = new Params();
        SkladkiUS = (min(x * 12, p.MaxSocialContBase) * (p.RetCont + p.PenCont) + x * 12 * p.DiseaseInsCont);
        skladkiUZ = (x * 12 - SkladkiUS) * p.HealthInsCont;
        podstawa = x * 12 - SkladkiUS - p.Tax;

        if (podstawa <= p.tax_1)
            zmn = p.taxAmount1;
        else if (podstawa <= p.tax_2)
            zmn = p.taxAmount1 - (p.taxAmount2 * (podstawa - p.tax_1) / p.taxDed2);
        else if (podstawa <= p.tax_3)
            zmn = p.taxAmount3;
        else if (podstawa <= p.tax_4)
            zmn = p.taxAmount3 - (p.taxAmount3 * (podstawa - p.tax_3) / p.taxDed4);
        else zmn = 0;
        if (podstawa <= p.tax_3)
            podatek = max(p.taxRateLevel1 * podstawa - zmn, 0);
        else
            podatek = max((p.taxRateLevel1 * p.tax_3) + p.taxRateLevel2 * (podstawa - p.tax_3) - zmn, 0);


        zaliczkaPD = max(podatek - p.HealthInsCont2 * (x * 12 - SkladkiUS), 0);
        wynikUPRAC = (x * 12 - SkladkiUS - skladkiUZ - zaliczkaPD) / 12;

        return wynikUPRAC;
    }

    static double Uzlec(double x, int a, int b, int c, double d) {
        double wynikUZLEC;
        double SkladkiUS;
        double skladkiUZ;
        double zaliczkaPD;
        double podatek;
        double podstawa;
        double zmn;
        double KosztyUzyskania;

        //Parametry
        Params p = new Params();


        SkladkiUS = (min(x * 12, p.MaxSocialContBase) * (p.RetCont + p.PenCont) * a + x * 12 * p.DiseaseInsCont * b);
        skladkiUZ = (x * 12 - SkladkiUS) * p.HealthInsCont * c;
        KosztyUzyskania = (x * 12 - SkladkiUS) * (d / 100);
        podstawa = x * 12 - SkladkiUS - KosztyUzyskania;

        if (podstawa <= p.tax_1)
            zmn = p.taxAmount1;
        else if (podstawa <= p.tax_2)
            zmn = p.taxAmount1 - (p.taxAmount2 * (podstawa - p.tax_1) / p.taxDed2);
        else if (podstawa <= p.tax_3)
            zmn = p.taxAmount3;
        else if (podstawa <= p.tax_4)
            zmn = p.taxAmount3 - (p.taxAmount3 * (podstawa - p.tax_3) / p.taxDed4);
        else zmn = 0;

        if (podstawa <= p.tax_3)
            podatek = max(p.taxRateLevel1 * podstawa - zmn, 0);
        else
            podatek = max((p.taxRateLevel1 * p.tax_3) + p.taxRateLevel2 * (podstawa - p.tax_3) - zmn, 0);

        zaliczkaPD = max(podatek - p.HealthInsCont2 * (x * 12 - SkladkiUS), 0);
        wynikUZLEC = (x * 12 - SkladkiUS - skladkiUZ - zaliczkaPD) / 12;

        return wynikUZLEC;
    }

    static double Udzg(double x) {
        return 1 * x;
    }

    static String slownik(String EmploymentType) {
        String wynik;

        switch (EmploymentType) {
            case "RZA_UPR":
                wynik = "Umowa o pracę";
                break;
            case "RZA_ZLEC_20":
                wynik = "Umowa zlecenie (stawka uzyskania przychodu 20%)";
                break;
            case "RZA_ZLEC_50":
                wynik = "Umowa zlecenie (stawka uzyskania przychodu 50%)";
                break;
            case "RZA_UDZ_20":
                wynik = "Umowa o dzieło (stawka uzyskania przychodu 20%)";
                break;
            case "RZA_UDZ_50":
                wynik = "Umowa o dzieło (stawka uzyskania przychodu 50%)";
                break;
            case "RZA_DZG":
                wynik = "Jednoosobowa działalność gospodarcza";
                break;

            default:
                wynik = "Brak";
        }
        return wynik;
    }

    @Override
    public Double calculateNetIncomeForEmployeeContract(Income income) {
        Data d1 = new Data();


        d1.EmpType = "RZA_UPR";
        d1.Income = income.getGrossIncome();

        d1.IsRent = 0; // dla UPRAC może być puste lub 0 / checkbox "dobrowolnie płacę składki emreytalno/rentowe"
        d1.IsDis = 0; // dla UPRAC zawsze 0 / checkbox "dobrowolnie płacę składki chorobowe"
        d1.IsHealth = 0; // dla UPRAC zawsze 0 / checkbox "dobrowolnie płacę składki zdrowotne"

        // wynik
        double res = 0.;

        if (!slownik(d1.EmpType).equals("Brak")) {
            res = NettoCalc(d1.EmpType, d1.Income, d1.IsRent, d1.IsDis, d1.IsHealth);
            System.out.println("Rodzaj zatrudnienia: " + slownik(d1.EmpType) + ". Dochód netto wynosi: " + res);
        } else {
            System.out.println("Błąd.");
        }

        return (double) Math.round(res * 100d) / 100;
    }

    @Override
    public Double calculateNetIncomeForCivilContract(Income income) {

        Data d1 = new Data();
        d1.EmpType = income.getRevenueRate().equals("20") ? "RZA_UDZ_20" : "RZA_UDZ_50";
        d1.Income = income.getGrossIncome();
        d1.IsRent = "TAK".equals(income.getRent()) ? 1 : 0; // dla UPRAC może być puste lub 0 / checkbox "dobrowolnie płacę składki emreytalno/rentowe"
        d1.IsDis = "TAK".equals(income.getDis()) ? 1 : 0; // dla UPRAC zawsze 0 / checkbox "dobrowolnie płacę składki chorobowe"
        d1.IsHealth = "TAK".equals(income.getHealth()) ? 1 : 0; // dla UPRAC zawsze 0 / checkbox "dobrowolnie płacę składki zdrowotne"


        // wynik
        double res = 0;

        if (slownik(d1.EmpType) != "Brak") {
            res = NettoCalc(d1.EmpType, d1.Income, d1.IsRent, d1.IsDis, d1.IsHealth);
            System.out.println("Rodzaj zatrudnienia: " + slownik(d1.EmpType) + ". Dochód netto wynosi: " + res);
        } else {
            System.out.println("Błąd.");
        }
        return (double) Math.round(res * 100d) / 100;


    }

    @Override
    public Double calculateNetForContractWork(Income income) {
        Data d1 = new Data();
        d1.EmpType = income.getRevenueRate().equals("20") ? "RZA_ZLEC_20" : "RZA_ZLEC_50";
        d1.Income = income.getGrossIncome();
        d1.IsRent = "TAK".equals(income.getRent()) ? 1 : 0; // dla UPRAC może być puste lub 0 / checkbox "dobrowolnie płacę składki emreytalno/rentowe"
        d1.IsDis = "TAK".equals(income.getDis()) ? 1 : 0; // dla UPRAC zawsze 0 / checkbox "dobrowolnie płacę składki chorobowe"
        d1.IsHealth = "TAK".equals(income.getHealth()) ? 1 : 0; // dla UPRAC zawsze 0 / checkbox "dobrowolnie płacę składki zdrowotne"


        // wynik
        double res = 0;

        if (slownik(d1.EmpType) != "Brak") {
            res = NettoCalc(d1.EmpType, d1.Income, d1.IsRent, d1.IsDis, d1.IsHealth);
            System.out.println("Rodzaj zatrudnienia: " + slownik(d1.EmpType) + ". Dochód netto wynosi: " + res);
        } else {
            System.out.println("Błąd.");
        }
        return (double) Math.round(res * 100d) / 100;
    }
}




