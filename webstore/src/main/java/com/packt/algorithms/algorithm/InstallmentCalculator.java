package com.packt.algorithms.algorithm;

import com.packt.algorithms.entities.Installment;

public interface InstallmentCalculator {
    Double calculate(Installment installment);

}
