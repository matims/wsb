package com.packt.algorithms.algorithm;

import com.packt.calc.domain.Income;

public abstract class Calculation {
    public abstract Double calculateNetIncomeForEmployeeContract(Income income);

    public abstract Double calculateNetIncomeForCivilContract(Income income);

    public abstract Double calculateNetForContractWork(Income income);
}
