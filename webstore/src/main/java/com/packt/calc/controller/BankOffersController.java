package com.packt.calc.controller;

import com.html.impl.BankOfferResponseHelperImpl;
import com.jdbc.ConnectJDBC;
import com.packt.calc.domain.BankOffer;
import com.r.CallRScript;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;


@Controller
@RequestMapping("/offers")
public class BankOffersController {

    private List<BankOffer> bankOfferList = new ArrayList();
    int probe = 0;
    CallRScript r;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public String getAddNewBankOfferForm(@ModelAttribute("newBankOffer") BankOffer newBankOffer) {


        return "offers";
    }


    @ModelAttribute("periods")
    public List<String> getPeriods() {
        List periods = new ArrayList();

        periods.add("6");
        periods.add("12");
        periods.add("18");
        periods.add("24");
        periods.add("30");
        periods.add("36");
        periods.add("42");
        periods.add("48");
        periods.add("54");
        periods.add("60");
        periods.add("66");
        periods.add("72");
        periods.add("78");
        periods.add("84");
        periods.add("90");
        periods.add("96");
        periods.add("102");
        periods.add("108");
        periods.add("114");
        periods.add("120");


        return periods;
    }

    @ModelAttribute("amounts")
    public List<String> getCreditAmounts() {

        List amounts = new ArrayList();
        amounts.add("1000");
        amounts.add("2000");
        amounts.add("3000");
        amounts.add("5000");
        amounts.add("6000");
        amounts.add("7000");
        amounts.add("8000");
        amounts.add("9000");
        amounts.add("10000");
        amounts.add("20000");
        amounts.add("30000");
        amounts.add("40000");
        amounts.add("50000");
        amounts.add("60000");
        amounts.add("70000");
        amounts.add("80000");
        amounts.add("90000");
        amounts.add("100000");
        amounts.add("110000");
        amounts.add("120000");
        amounts.add("130000");
        amounts.add("140000");
        amounts.add("150000");
        amounts.add("160000");
        amounts.add("170000");
        amounts.add("180000");
        amounts.add("190000");
        amounts.add("200000");

        return amounts;
    }


    @RequestMapping(value = "", method = RequestMethod.POST)
    public String processAddNewIncomeForm(@ModelAttribute("newBankOffer") BankOffer bankOfferToAdded, ModelMap map, BindingResult result) throws Exception {
        String[] suppressedFields = result.getSuppressedFields();

        if (suppressedFields.length > 0) {
            throw new RuntimeException("Próba wiązania niedozwolonych pól: " + StringUtils.arrayToCommaDelimitedString(suppressedFields));
        }

        String chosenCreditAmount = bankOfferToAdded.getChosenCreditAmount();
        String chosenPeriod = bankOfferToAdded.getChosenPeriod();
        //Logger;
        bankOfferList.stream().forEach(s -> System.out.println(s + " : "));
        bankOfferList = new BankOfferResponseHelperImpl().prepareBankOfferResponses(chosenCreditAmount, chosenPeriod);
        try {
            new ConnectJDBC().insertBankOffersToDatabase(bankOfferList);
            //Logger
            bankOfferList.stream().forEach(s -> System.out.println(s + " : "));
        } catch (Exception r) {
            //  new FillDataToCsvImpl().fillCsv(bankOfferList);
            System.out.println("Nie nawiązano połączenia z bazą danych.");
        }
        if (probe > 0) {
            bankOfferList.remove(0);
        }
        bankOfferList.stream().forEach(s -> System.out.println(s + " : "));

       /* r = new CallRScript();
        r.call(chosenCreditAmount, chosenPeriod);*/

        probe++;
        return "redirect:/offers/foundOffers";
    }

    @InitBinder
    public void initialiseBinder(WebDataBinder binder) {
        binder.setAllowedFields("chosenCreditAmount", "chosenPeriod");
    }

    @RequestMapping("/foundOffers")
    public String result(Model model) {
        model.addAttribute("offers", bankOfferList);
        model.addAttribute("chosenCreditAmount", bankOfferList.stream().findFirst().get().getChosenCreditAmount());
        model.addAttribute("chosenPeriod", bankOfferList.stream().findFirst().get().getChosenPeriod());
        return "foundOffers";
    }

    @RequestMapping("/dashboard")
    public String dashboard(Model model) {
        return "redirect:/pages/test_dash2.html";
    }




}
