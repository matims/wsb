package com.packt.calc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.packt.calc.service.FuncionalityService;


@Controller
public class HomeController {

    @Autowired
    FuncionalityService funcionalityService;

    @RequestMapping("/")
    public String welcome(Model model) {
        model.addAttribute("greeting", "Kalkulatory finansowe");
        model.addAttribute("tagline", "Wybierz funkcjonalność");
        model.addAttribute("choose", funcionalityService.getAllFuncionalities());

        return "welcome";
    }


}
