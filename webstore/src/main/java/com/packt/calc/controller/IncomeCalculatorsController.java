package com.packt.calc.controller;

import com.packt.algorithms.algorithm.impl.CalculationNetIncomeImpl;
import com.packt.algorithms.algorithm.impl.IncomeResponse;
import com.packt.calc.domain.Income;
import com.packt.calc.service.IncomeCalculatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/calculators")
public class IncomeCalculatorsController {

    public Income incomeResonse = null;


    @Autowired
    IncomeCalculatorService incomeCalculatorService;

    @RequestMapping
    // default method in the class - removed annotations after RequestMapping
    public String list(Model model) {
        model.addAttribute("calculators", incomeCalculatorService.getAllCalculators());
        return "calculators";
    }

    @RequestMapping("/all")
    public ModelAndView allCalculators() {
        ModelAndView modelAndView = new ModelAndView();

        modelAndView.addObject("calculators", incomeCalculatorService.getAllCalculators());
        modelAndView.setViewName("calculators");
        return modelAndView;
    }

    @RequestMapping("/calculator")
    public String getCalculatorById(@RequestParam("id") String id, Model model) {
        model.addAttribute("calculator", incomeCalculatorService.getIncomeCalculatorById(id));
        return "calculator";
    }

    @RequestMapping(value = "/addIncome", method = RequestMethod.GET)
    public String getAddNewIncomeForm(@ModelAttribute("newIncome") Income newIncome) {


        return "addIncome";
    }

    @RequestMapping(value = "/addIncome", method = RequestMethod.POST)
    public String processAddNewIncomeForm(@ModelAttribute("newIncome") Income incomeToBeAdded, ModelMap map, BindingResult result) {
        String[] suppressedFields = result.getSuppressedFields();

        if (suppressedFields.length > 0) {
            throw new RuntimeException("Próba wiązania niedozwolonych pól: " + StringUtils.arrayToCommaDelimitedString(suppressedFields));
        }

        incomeResonse = new IncomeResponse().prepareIncomeResponse(incomeToBeAdded);
        //Logger
        System.out.println(incomeToBeAdded);

        return "redirect:/calculators/result";
    }


    @RequestMapping(value = "/addIncome1", method = RequestMethod.GET)
    public String getAddNewIncomeForm1(@ModelAttribute("newIncome") Income newIncome) {


        return "addIncome1";
    }

    @RequestMapping(value = "/addIncome1", method = RequestMethod.POST)
    public String processAddNewIncomeForm1(@ModelAttribute("newIncome") Income incomeToBeAdded, ModelMap map, BindingResult result) {
        String[] suppressedFields = result.getSuppressedFields();

        if (suppressedFields.length > 0) {
            throw new RuntimeException("Próba wiązania niedozwolonych pól: " + StringUtils.arrayToCommaDelimitedString(suppressedFields));
        }

        incomeResonse = new IncomeResponse().prepareIncomeResponse(incomeToBeAdded);
        System.out.println(incomeToBeAdded);

        return "redirect:/calculators/result1";
    }


    @RequestMapping(value = "/addIncome2", method = RequestMethod.GET)
    public String getAddNewIncomeForm2(@ModelAttribute("newIncome") Income newIncome) {


        return "addIncome2";
    }

    @RequestMapping(value = "/addIncome2", method = RequestMethod.POST)
    public String processAddNewIncomeForm2(@ModelAttribute("newIncome") Income incomeToBeAdded, ModelMap map, BindingResult result) {
        String[] suppressedFields = result.getSuppressedFields();

        if (suppressedFields.length > 0) {
            throw new RuntimeException("Próba wiązania niedozwolonych pól: " + StringUtils.arrayToCommaDelimitedString(suppressedFields));
        }

        incomeResonse = new IncomeResponse().prepareIncomeResponse(incomeToBeAdded);
        return "redirect:/calculators/result2";
    }

    @InitBinder
    public void initialiseBinder(WebDataBinder binder) {
        binder.setAllowedFields("grossIncome", "authorialCosts", "revenueRate", "dis", "health", "rent");

    }

    @RequestMapping("/result")
    public String result(Model model) {
        model.addAttribute("netIncome", new CalculationNetIncomeImpl().calculateNetIncomeForEmployeeContract(incomeResonse));

        return "result";
    }

    @RequestMapping("/result1")
    public String result1(Model model) {
        model.addAttribute("netIncome", new CalculationNetIncomeImpl().calculateNetForContractWork(incomeResonse));

        return "result1";
    }

    @RequestMapping("/result2")
    public String result2(Model model) {
        model.addAttribute("netIncome", new CalculationNetIncomeImpl().calculateNetIncomeForCivilContract(incomeResonse));

        return "result2";
    }


}
