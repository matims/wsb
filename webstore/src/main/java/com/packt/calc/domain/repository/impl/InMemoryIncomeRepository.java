package com.packt.calc.domain.repository.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.packt.calc.domain.repository.IncomeRepository;

import com.packt.calc.domain.Income;

@Repository
public class InMemoryIncomeRepository implements IncomeRepository {

    List<Income> incomesList = new ArrayList();

    public InMemoryIncomeRepository() {
        // TODO Auto-generated constructor stub
    }

    @Override
    public void addIncome(Income income) {
        System.out.println("addIncome" + income);
        incomesList.add(income);

    }

    @Override
    public List<Income> getAllIncome() {
        return incomesList;
    }

}
