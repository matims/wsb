package com.packt.calc.domain;

public class IncomeCalculator {

    private String id;
    private String nameOfFuncionality;
    private String description;
    private String site;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public String getNameOfFuncionality() {
        return nameOfFuncionality;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setNameOfFuncionality(String nameOfFuncionality) {
        this.nameOfFuncionality = nameOfFuncionality;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public IncomeCalculator() {
        super();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }

}
