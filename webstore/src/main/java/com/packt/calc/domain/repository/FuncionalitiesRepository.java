package com.packt.calc.domain.repository;

import java.util.List;

import com.packt.calc.domain.Funcionality;

public interface FuncionalitiesRepository {

    List<Funcionality> getAllFuncionalities();

}
