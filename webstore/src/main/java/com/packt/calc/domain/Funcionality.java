package com.packt.calc.domain;


public class Funcionality {

    public final static String INCOMES_CALCULATOR = "Kalkulator dochodowy";
    public final static String INSTALLEMENT_CALCULATOR = "Porownywarka kredytów gotowkowych";

    private String site;
    private String nameOfFuncionality;
    private String description;

    public Funcionality() {
        super();
    }

    public String getNameOfFuncionality() {
        return nameOfFuncionality;
    }

    public void setNameOfFuncionality(String nameOfFuncionality) {
        this.nameOfFuncionality = nameOfFuncionality;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    @Override
    public boolean equals(Object obj) {
        // TODO Auto-generated method stub
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        // TODO Auto-generated method stub
        return super.hashCode();
    }

    @Override
    public String toString() {
        // TODO Auto-generated method stub
        return super.toString();
    }


}
