package com.packt.calc.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BankOffer {

    private String titleOfOffer;
    private String RRSO;
    private String creditAmount;
    private String installmentAmount;
    private String bankName;
    private String chosenPeriod;
    private String chosenCreditAmount;
    private int id;
    private Integer bankId;
    private Date checkDate;
    private String wwwBankPage;
    private List<MappingBankName> mappingBanks = new ArrayList();

    {
        mappingBanks.add(new MappingBankName("Nest Bank", 1, "https://nestbank.pl/"));
        mappingBanks.add(new MappingBankName("Santander Bank Polska", 2, "https://santander.pl/"));
        mappingBanks.add(new MappingBankName("Alior Bank", 3, "https://www.aliorbank.pl/"));
        mappingBanks.add(new MappingBankName("Bank BPS", 4, "https://www.bankbps.pl/"));
        mappingBanks.add(new MappingBankName("Bank PBS", 5, "https://www.pbsbank.pl/"));
        mappingBanks.add(new MappingBankName("Bank Pekao", 6, "https://www.pekao.com.pl/"));
        mappingBanks.add(new MappingBankName("Bank Pocztowy", 7, "https://www.pocztowy.pl/"));
        mappingBanks.add(new MappingBankName("BNP Paribas", 8, "https://www.bnpparibas.pl/"));
        mappingBanks.add(new MappingBankName("BO", 9, "https://www.bosbank.pl/"));
        mappingBanks.add(new MappingBankName("Citi Handlowy", 10, "https://www.online.citibank.pl/"));
        mappingBanks.add(new MappingBankName("Agricole", 11, "https://www.credit-agricole.pl/"));
        mappingBanks.add(new MappingBankName("Eurobank", 12, "https://eurobank.pl/"));
        mappingBanks.add(new MappingBankName("Getin Noble Bank", 13, "https://www.getinbank.pl/"));
        mappingBanks.add(new MappingBankName("Idea Bank", 14, "https://www.ideabank.pl/"));
        mappingBanks.add(new MappingBankName("ING", 15, "https://www.ing.pl/"));
        mappingBanks.add(new MappingBankName("mBank", 16, "https://www.mbank.pl/"));
        mappingBanks.add(new MappingBankName("Millennium", 17, "https://www.bankmillennium.pl/"));
        mappingBanks.add(new MappingBankName("PKO Bank Polski", 18, "https://www.pkobp.pl/"));
        mappingBanks.add(new MappingBankName("PLUS BANK", 19, "https://plusbank.pl/"));
        mappingBanks.add(new MappingBankName("Santander Consumer Bank", 20, "https://www.santanderconsumer.pl/"));
        mappingBanks.add(new MappingBankName("T-Mobile", 21, "https://www.t-mobilebankowe.pl/"));
    }


    public void setWwwBankPage(String bankName) {
        if (bankName == null) {
            return;
        }
        try {
            this.wwwBankPage = new MappingBankName(bankName).wwwBankPage;
        } catch (Exception e) {
            this.wwwBankPage = null;
        }
    }

    public String getWwwBankPage() {
        return wwwBankPage;
    }

    public void setBankId(String bankName) {

        if (bankName == null) {
            return;
        }
        try {
            this.bankId = new MappingBankName(bankName).bankId;
        } catch (Exception e) {
            this.bankId = 0;
        }
    }

    public Integer getBankId() {
        return bankId;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getChosenCreditAmount() {
        return chosenCreditAmount;
    }

    public String getChosenPeriod() {
        return chosenPeriod;
    }

    public void setChosenCreditAmount(String chosenCreditAmount) {
        this.chosenCreditAmount = chosenCreditAmount;
    }

    public void setChosenPeriod(String chosenPeriod) {
        this.chosenPeriod = chosenPeriod;
    }

    public String getBankName() {
        return bankName;
    }

    public String getCreditAmount() {
        return creditAmount;
    }

    public String getInstallmentAmount() {
        return installmentAmount;
    }

    public String getRRSO() {
        return RRSO;
    }

    public String getTitleOfOffer() {
        return titleOfOffer;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public void setCreditAmount(String creditAmount) {
        this.creditAmount = creditAmount;
    }

    public void setInstallmentAmount(String installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    public void setRRSO(String RRSO) {
        this.RRSO = RRSO;
    }

    public void setTitleOfOffer(String titleOfOffer) {
        this.titleOfOffer = titleOfOffer;
    }

    @Override
    public String toString() {
        return getBankName() + " " + getBankId() + " " + getTitleOfOffer() + " " + getRRSO() + " " + getCreditAmount() + " " + getInstallmentAmount() + " " + getChosenCreditAmount() + " " + getChosenPeriod() + " " + getWwwBankPage();
    }


    private class MappingBankName {
        private String bankName;
        private Integer bankId;
        private String wwwBankPage;

        private MappingBankName(String bankName, Integer bankId, String wwwBankPage) {
            this.bankId = bankId;
            this.bankName = bankName;
            this.wwwBankPage = wwwBankPage;
        }

        private MappingBankName(String bankName) {
            this.bankName = bankName;
            this.bankId = getBankId(bankName);
            this.wwwBankPage = getWWWBankPage(bankName);
        }

        Integer getBankId(String fullBankName) {
            return mappingBanks.stream().filter(b -> fullBankName.contains(b.getBankName())).findFirst().get().bankId;
        }

        String getWWWBankPage(String fullBankName) {
            return mappingBanks.stream().filter(b -> fullBankName.contains(b.getBankName())).findFirst().get().wwwBankPage;
        }

        String getBankName() {
            return bankName;
        }
    }


}
