package com.packt.calc.domain.repository.impl;

import com.packt.calc.domain.Funcionality;
import com.packt.calc.domain.repository.FuncionalitiesRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import static com.packt.calc.domain.Funcionality.INCOMES_CALCULATOR;
import static com.packt.calc.domain.Funcionality.INSTALLEMENT_CALCULATOR;


@Repository
public class InMemoryFunctionalityRepository implements FuncionalitiesRepository {


    private List<Funcionality> listOfFuncionalities = new ArrayList<Funcionality>();

    @Override
    public List<Funcionality> getAllFuncionalities() {
        clearCurrentList(listOfFuncionalities);
        Funcionality funcionality = new Funcionality();
        funcionality.setNameOfFuncionality(INCOMES_CALCULATOR);
        funcionality.setDescription("Kalkulator wyliczajacy dochod netto dla wybranych rodzajow zatrudnienia.");
        funcionality.setSite("calculators");
        listOfFuncionalities.add(funcionality);

        Funcionality funcionality1 = new Funcionality();
        funcionality1.setNameOfFuncionality(INSTALLEMENT_CALCULATOR);
        funcionality1.setDescription("Zestawienie ofert kredytowych Banków");
        funcionality1.setSite("offers");
        listOfFuncionalities.add(funcionality1);

        return listOfFuncionalities;
    }

    private void clearCurrentList(List list) {
        list.clear();
    }

}
