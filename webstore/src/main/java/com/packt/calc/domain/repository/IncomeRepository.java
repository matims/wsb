package com.packt.calc.domain.repository;

import com.packt.calc.domain.Income;

import java.util.List;

public interface IncomeRepository {

    void addIncome(Income income);

    List<Income> getAllIncome();

}
