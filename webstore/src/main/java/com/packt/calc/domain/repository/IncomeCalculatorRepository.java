package com.packt.calc.domain.repository;

import com.packt.calc.domain.IncomeCalculator;

import java.util.List;

public interface IncomeCalculatorRepository {

    List<IncomeCalculator> getAllCalculators();

    IncomeCalculator getIncomeCalculatorById(String calculatorID);

    List<IncomeCalculator> getIncomeCalculatorByName(String name);


}
