package com.packt.calc.domain.repository.impl;

import com.packt.calc.domain.IncomeCalculator;
import com.packt.calc.domain.repository.IncomeCalculatorRepository;
import com.packt.dict.EmploymentType;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class InMemoryIncomeCalculatorRepository implements IncomeCalculatorRepository {

    private List<IncomeCalculator> listOfProducts = new ArrayList<IncomeCalculator>();

    public InMemoryIncomeCalculatorRepository() {

        IncomeCalculator inc = new IncomeCalculator();
        inc.setNameOfFuncionality(EmploymentType.EMPLOYMENT_CONTRACT);
        inc.setDescription("Wyliczenie dochodu netto");
        inc.setSite("addIncome");
        inc.setId("1");

        listOfProducts.add(inc);

        IncomeCalculator inc1 = new IncomeCalculator();
        inc1.setNameOfFuncionality(EmploymentType.CIVIL_CONTACT);
        inc1.setDescription("Wyliczenie dochodu netto");
        inc1.setSite("addIncome1");
        inc1.setId("2");

        listOfProducts.add(inc1);

        IncomeCalculator inc2 = new IncomeCalculator();
        inc2.setNameOfFuncionality(EmploymentType.CONTRACT_WORK);
        inc2.setDescription("Wyliczenie dochodu netto");
        inc2.setSite("addIncome2");
        inc2.setId("3");

        listOfProducts.add(inc2);

       /* IncomeCalculator inc3 = new IncomeCalculator();
        inc3.setNameOfFuncionality(EmploymentType.SELF_EMPLOYED);
        inc3.setDescription("Wyliczenie dochodu netto");
        inc3.setSite("addIncome3");
        inc3.setId("4");

        listOfProducts.add(inc3);*/

    }

    @Override
    public List<IncomeCalculator> getAllCalculators() {

        return listOfProducts;
    }

    @Override
    public IncomeCalculator getIncomeCalculatorById(final String calculatorID) {

        return listOfProducts.stream().filter(ic -> calculatorID.equals(ic.getId())).findFirst().orElse(null);

    }

    @Override
    public List<IncomeCalculator> getIncomeCalculatorByName(String name) {

        return listOfProducts.stream().filter(ic -> name.equals(ic.getNameOfFuncionality()))
                .collect(Collectors.toList());

    }

}
