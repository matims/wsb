package com.packt.calc.domain;

import java.io.Serializable;

public class Income implements Serializable {

    private static final long serialVersionUID = 1L;
    private long incomeId;
    private Double grossIncome;
    private String taxYear;
    private Double rate;
    private Double procentOfSocialSecurity;
    private String authorialCosts = "NIE";
    private Double procentOfAuthorialCosts;
    private Double netIncome;
    private String chosenFuncionality;
    private String revenueRate;
    private String rent = "NIE";
    private String dis = "NIE";
    private String health = "NIE";


    public String getRevenueRate() {
        return revenueRate;
    }

    public void setRevenueRate(String revenueRate) {
        this.revenueRate = revenueRate;
    }

    public Double getProcentOfSocialSecurity() {
        return procentOfSocialSecurity;
    }

    public void setProcentOfSocialSecurity(Double procentOfSocialSecurity) {
        this.procentOfSocialSecurity = procentOfSocialSecurity;
    }

    public String getChosenFuncionality() {
        return chosenFuncionality;
    }

    public void setChosenFuncionality(String chosenFuncionality) {
        this.chosenFuncionality = chosenFuncionality;
    }

    public Double getNetIncome() {
        return netIncome;
    }

    public void setNetIncome(Double netIncome) {
        this.netIncome = netIncome;
    }

    public void setAuthorialCosts(String authorialCosts) {
        this.authorialCosts = authorialCosts;
    }

    public String getAuthorialCosts() {
        return authorialCosts;
    }

    public Double getProcentOfAuthorialCosts() {
        return procentOfAuthorialCosts;
    }

    public void setProcentOfAuthorialCosts(Double procentOfAuthorialCosts) {
        this.procentOfAuthorialCosts = procentOfAuthorialCosts;
    }

    public void setIncomeId(long incomeId) {
        this.incomeId = incomeId;
    }

    public void setGrossIncome(Double grossIncome) {
        this.grossIncome = grossIncome;
    }

    public void setTaxYear(String taxYear) {
        this.taxYear = taxYear;
    }

    public Double getGrossIncome() {
        return grossIncome;
    }

    public long getIncomeId() {
        return incomeId;
    }

    public Double getRate() {
        return rate;
    }

    public String getTaxYear() {
        return taxYear;
    }

    public String getDis() {
        return dis;
    }

    public String getHealth() {
        return health;
    }

    public String getRent() {
        return rent;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public void setDis(String dis) {
        this.dis = dis;
    }

    public void setHealth(String health) {
        this.health = health;
    }

    public void setRent(String rent) {
        this.rent = rent;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return "[income]" + getGrossIncome() + "/" + getNetIncome() + "/" + getRevenueRate() + "/" + getDis() + "/" + getHealth() + "/" + getRent();
    }


}
