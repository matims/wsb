package com.packt.calc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.packt.calc.domain.Funcionality;
import com.packt.calc.domain.repository.FuncionalitiesRepository;
import com.packt.calc.service.FuncionalityService;

@Service
public class FuncionalityServiceImpl implements FuncionalityService {

    @Autowired
    FuncionalitiesRepository funcionalitiesRepository;

    public List<Funcionality> getAllFuncionalities() {
        return funcionalitiesRepository.getAllFuncionalities();
    }

}
