package com.packt.calc.service;

import java.util.List;

import com.packt.calc.domain.Funcionality;

public interface FuncionalityService {
    List<Funcionality> getAllFuncionalities();
}
