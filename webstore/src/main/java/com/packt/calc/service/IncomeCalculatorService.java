package com.packt.calc.service;

import java.util.List;

import com.packt.calc.domain.IncomeCalculator;

public interface IncomeCalculatorService {
    List<IncomeCalculator> getAllCalculators();

    IncomeCalculator getIncomeCalculatorById(String id);

    List<IncomeCalculator> getIncomeCalculatorByName(String name);
}
