package com.packt.calc.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.packt.calc.domain.IncomeCalculator;
import com.packt.calc.domain.repository.IncomeCalculatorRepository;
import com.packt.calc.service.IncomeCalculatorService;

@Service
public class IncomeCalculatorServiceImpl implements IncomeCalculatorService {

    @Autowired
    IncomeCalculatorRepository incomeRepository;

    @Override
    public List<IncomeCalculator> getAllCalculators() {
        return incomeRepository.getAllCalculators();
    }

    @Override
    public IncomeCalculator getIncomeCalculatorById(String calculatorID) {
        return incomeRepository.getIncomeCalculatorById(calculatorID);
    }

    @Override
    public List<IncomeCalculator> getIncomeCalculatorByName(String name) {
        return incomeRepository.getIncomeCalculatorByName(name);
    }

}
