package com.packt.calc.service;

import java.util.List;

import com.packt.calc.domain.Income;

public interface IncomeService {

    void addIncome(Income income);

    List<Income> getAllIncome();

}
