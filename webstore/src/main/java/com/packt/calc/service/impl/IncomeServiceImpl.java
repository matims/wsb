package com.packt.calc.service.impl;

import com.packt.calc.domain.Income;
import com.packt.calc.domain.repository.IncomeRepository;
import com.packt.calc.service.IncomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IncomeServiceImpl implements IncomeService {

    @Autowired
    IncomeRepository incomeRepository;

    @Override
    public void addIncome(Income income) {
        incomeRepository.addIncome(income);

    }

    @Override
    public List<Income> getAllIncome() {
        return incomeRepository.getAllIncome();
    }

}
