package com.html;

import java.net.URL;

public interface DownloadDataFromHtml {

    public String getHTML(URL url) throws Exception;
}
