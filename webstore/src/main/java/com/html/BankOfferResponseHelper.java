package com.html;

import com.packt.calc.domain.BankOffer;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public abstract class BankOfferResponseHelper {

    protected String bankName = null;
    protected String offerTitle = null;
    protected String rrso = null;
    protected String initialAmount = null;
    protected String installmentAmount = null;
    protected List<BankOffer> bankOffers = new ArrayList<>();

    abstract protected BankOffer prepareBankOffer(String bankName, String offerTitle, String rrso, String initialAmount, String installmentAmount, String chosenInitialAmount, String chosenInstallmentNumber);

    abstract protected String getBankName(String offer);

    abstract protected String getOfferTitle(String offer);

    abstract protected String getRrso(String offer);

    abstract protected String getInitialAmount(String offer);

    abstract protected String getInstallmentAmount(String offer);

    protected List<BankOffer> distinctDataByTitleOfferAndBankName(List<BankOffer> bankOffers) {
        return bankOffers.stream()
                .filter(distinctByKey(p -> p.getTitleOfOffer() + " " + p.getBankName()))
                .collect(Collectors.toList());
    }

    abstract public List<BankOffer> prepareBankOfferResponses(String chosenInitialAmount, String chosenInstallmentNumber) throws Exception;


    public static void removeEmptyRRSOFromList(List<BankOffer> bankOffers) {
        bankOffers.removeIf(b -> b.getRRSO() == null);

    }

    protected List<String> cleanText(String text) {
        String delString = "col-12 col-lg order-2 order-sm-3 order-lg-2 mt-3 mt-lg-0";
        String delString1 = "col-12 col-sm-6 col-lg-3 pr-lg-5 order-3 order-sm-2 order-lg-3";
        String[] split = text.replace(delString, "").replace(delString1, "").split("alt=");
        ArrayList offers = new ArrayList();
        for (String s : split) {
            String delString2 = "class=adsMarker>";
            String delString3 = "<p>Oferta sponsorowana</p>";
            if (!(s.contains(delString2) || s.contains(delString3))) {
                offers.add(s);
            }
        }
        return offers;
    }

    protected int findNextQuote(char[] chars, int startIndex, char specialChar, int count) {
        int start = 0;
        for (int i = startIndex; i < chars.length; i++) {
            if (chars[i] == specialChar) {
                start++;
            }
            if (start > count) {
                return i;
            }

        }
        return startIndex;
    }

    private static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
        Set<Object> seen = ConcurrentHashMap.newKeySet();
        return t -> seen.add(keyExtractor.apply(t));
    }

}