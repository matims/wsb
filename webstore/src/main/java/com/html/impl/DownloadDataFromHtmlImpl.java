package com.html.impl;

import com.html.DownloadDataFromHtml;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class DownloadDataFromHtmlImpl implements DownloadDataFromHtml {

    String temp;

    public String getHTML(URL url) throws Exception {

        BufferedReader download = new BufferedReader(new InputStreamReader(url.openStream()));

        StringBuilder s = new StringBuilder();
        while ((temp = download.readLine()) != null) {
            s.append(temp).append("\n");
        }
        download.close();
        return s.toString();
    }
}
