package com.html.impl;

import com.html.BankOfferResponseHelper;
import com.packt.calc.domain.BankOffer;

import java.net.URL;
import java.util.List;

public class BankOfferResponseHelperImpl extends BankOfferResponseHelper {


    @Override
    public List<BankOffer> prepareBankOfferResponses(String chosenInitialAmount, String chosenInstallmentNumber) throws Exception {

        String html = new DownloadDataFromHtmlImpl().getHTML(new URL("https://www.comperia.pl/kredyty_gotowkowe/lista-ofert,kwota-" + chosenInitialAmount + "-zl,okres-" + chosenInstallmentNumber + "-mies,rrso-rosnaco,strona-100"));
        List<String> offers = cleanText(html);
        bankOffers.clear();
        for (String offer : offers) {
            bankOffers.add(prepareBankOffer(getBankName(offer), getOfferTitle(offer), getRrso(offer), getInitialAmount(offer), getInstallmentAmount(offer), chosenInitialAmount, chosenInstallmentNumber));
        }
        removeEmptyRRSOFromList(bankOffers);

        return distinctDataByTitleOfferAndBankName(bankOffers);

    }

    @Override
    protected String getBankName(String offer) {
        String bankNamePattern = "data-bank=";
        return offer.indexOf(bankNamePattern) > 0 ? offer.substring(offer.indexOf(bankNamePattern) + 11, findNextQuote(offer.toCharArray(), offer.indexOf(bankNamePattern) - 1, '"', 1)) : bankName;

    }

    @Override
    protected String getOfferTitle(String offer) {
        String bankTitleOfOfferPattern = "data-produkt";
        return offer.indexOf(bankTitleOfOfferPattern) > 0 ? offer.substring(offer.indexOf(bankTitleOfOfferPattern) + 14, findNextQuote(offer.toCharArray(), offer.indexOf(bankTitleOfOfferPattern) - 1, '"', 1)) : offerTitle;

    }

    @Override
    protected String getRrso(String offer) {
        String bankRrsoPattern = "RRSO";
        return offer.indexOf(bankRrsoPattern) > 0 ? offer.substring(offer.indexOf(bankRrsoPattern), findNextQuote(offer.toCharArray(), offer.indexOf(bankRrsoPattern), '<', 0)) : rrso;
    }

    @Override
    protected String getInitialAmount(String offer) {
        String bankInitialAmount1Pattern = "aty:";
        return offer.indexOf(bankInitialAmount1Pattern) > 0 ? offer.substring(offer.indexOf(bankInitialAmount1Pattern) + 4, findNextQuote(offer.toCharArray(), offer.indexOf(bankInitialAmount1Pattern), '<', 0)) : initialAmount;
    }

    @Override
    protected String getInstallmentAmount(String offer) {
        String bankInsAmountPattern = "Rata:";
        return offer.indexOf(bankInsAmountPattern) > 0 ? offer.substring(offer.indexOf(bankInsAmountPattern) + 5, findNextQuote(offer.toCharArray(), offer.indexOf(bankInsAmountPattern), '<', 0)) : installmentAmount;
    }

    @Override
    protected BankOffer prepareBankOffer(String bankName, String offerTitle, String rrso, String initialAmount, String installmentAmount, String chosenInitialAmount, String chosenInstallmentNumber) {
        BankOffer bankOffer = new BankOffer();
        bankOffer.setBankName(bankName);
        bankOffer.setTitleOfOffer(offerTitle);
        bankOffer.setRRSO(rrso == null ? rrso : rrso.replace("RRSO: ", ""));
        bankOffer.setCreditAmount(initialAmount);
        bankOffer.setInstallmentAmount(installmentAmount);
        bankOffer.setChosenCreditAmount(chosenInitialAmount);
        bankOffer.setChosenPeriod(chosenInstallmentNumber);
        bankOffer.setBankId(bankName);
        bankOffer.setWwwBankPage(bankName);
        return bankOffer;
    }


}