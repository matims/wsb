<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">


<title>Najlepsze oferty</title>
</head>

<section>
		<div class="jumbotron" style="background-image:url('https://acnosoft.com/wp-content/uploads/2018/12/WordPRess-Website-Design-Banner.jpg');;padding:50px;width:1400px;height:200px;border:1px solid black;">
			<div class="container">


			</div>
		</div>

		                <p>
        					<a href="/webstore/offers" class="btn btn-default"> <span
        						class="glyphicon-hand-left glyphicon"></span> Powrót
        					</a>
        				</p>


                        <p>
        					<a href="/webstore/offers/dashboard" class="btn btn-default btn-lg"> <span
        						class="glyphicon glyphicon-picture"></span> Analiza ofert
        					</a>
        				</p>



	</section>






<body bgcolor="teal" text="black">
	<section class="container">


    				<legend>Najlepsze oferty Banków dla wybranych warunków ${chosenCreditAmount} zł na okres ${chosenPeriod} rat.</legend>


		<div class="row">
			<c:forEach items="${offers}" var="bankOffer">
           		<div class="col-sm-8 col-md-8" style="padding-bottom: 25px">
                <div class="thumbnail">
                            <font face = "WildWest" size = "5">
                           <br><p>${bankOffer.bankName}</p></br>
							</font>
							<img src="<c:url value="/resource/images/${bankOffer.bankId}.png"></c:url>" alt="image"  style = "width:30%"/>
							 <font face = "WildWest" size = "3">
							<p>${bankOffer.titleOfOffer} </p>
							<p>RRSO ${bankOffer.RRSO}</p>
							<p>Kwota do spłaty ${bankOffer.creditAmount}</p>
							<p>Kwota miesięcznej raty ${bankOffer.installmentAmount}</p>
							<a href=${bankOffer.wwwBankPage}>Szczegóły oferty</a>
							</font><br />
						</div>
				</div>

			</c:forEach>
		</div>
	</section>

</body>
</html>
