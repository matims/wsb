<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<title>Kalkulator</title>
</head>
<body>
	<section>
		<div class="jumbotron">
			<div class="container">
				<h1>Kalkulator</h1>
			</div>
		</div>
	</section>
	<section class="container">
		<div class="row">
			<div class="col-md-8">
				<h3>${calculator.nameOfFuncionality}</h3>
				<p>${calculator.description}</p>


				<p>
					<a href="/webstore/calculators" class="btn btn-default"> <span
						class="glyphicon-hand-left glyphicon"></span> Powrót
					</a>

				</p>

			</div>
		</div>
	</section>

		<section class="container">
		<form:form  modelAttribute="newIncome" class="form-horizontal">
			<fieldset>
				<legend>Uzupełnij dochód</legend>

			
				<div class="form-group">
					<label class="control-label col-lg-2" for="grossIncome">Podaj dochod brutto</label>
					<div class="col-lg-10">
						<form:input id="grossIncome" path="grossIncome" type="text" class="form:input-large"/>
					</div>
				</div>

				
				<div class="form-group">
					<label class="control-label col-lg-2" for="isAuthorialCosts">Stan</label>
					<div class="col-lg-10">
						<form:radiobutton path="isAuthorialCosts" value="true" />Nowy 				
					</div>
				</div>
				
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<input type="submit" id="btnAdd" class="btn btn-primary" value ="Dodaj"/>
					</div>
				</div>
				
			</fieldset>
		</form:form>
	</section>


</body>
</html>
