<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
	href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<title>Kalkulatory dochodowe</title>
</head>
<body>

	<section>
	
		<div class="jumbotron" style="background-image:url('https://acnosoft.com/wp-content/uploads/2018/12/WordPRess-Website-Design-Banner.jpg');padding:50px;width:1400px;height:200px;border:1px solid black;">
			<div class="container">
				<h1></h1>
				<p></p>
			</div>
		</div>
	</section>

	<section class="container">
		<div class="row">

		<legend>Wybierz kalkulator dochodowy</legend>

			<c:forEach items="${calculators}" var="calculator">
				<div class="col-md-8 nopadding">
						<div class="caption">
							<h3>${calculator.nameOfFuncionality}</h3>
							<p>${calculator.description}</p>
						
							<p>


							<a href=" <spring:url value="/calculators/${calculator.site}" /> "
                                    	class="btn btn-primary btn-lg active"
                                    	role="button">Wybierz</a><br>


							</p>

						</div>
				</div>
	<h1></h1>
			</c:forEach>
		</div>
		
		<h1></h1>
			<p>	
					 <a href="/webstore" class="btn btn-default">
						<span class="glyphicon-hand-left glyphicon"></span> Powrót
					</a>

				</p>
		
	</section>
</body>
</html>
