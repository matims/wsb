<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet"
    href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css">
<title>Pokaż oferty</title>
</head>
<body>
	<section>
		<div class="jumbotron" style="background-image:url('https://acnosoft.com/wp-content/uploads/2018/12/WordPRess-Website-Design-Banner.jpg');;padding:50px;width:1400px;height:200px;border:1px solid black;">
			<div class="container">

			</div>

		</div>
	</section>

	<section class="container">
		<form:form  modelAttribute="newBankOffer" class="form-horizontal">
			<fieldset>

<legend>Najlepsze oferty kredytów: </legend>
				<div class="form-group">
					<label class="control-label col-lg-2" for="chosenCreditAmount">Podaj kwote kredytu</label>
					<div class="col-lg-8">
						<form:select path = "chosenCreditAmount" items = "${amounts}"
                                                                 multiple = "false" />
					</div>
				</div>


             <div class="form-group">
					<label class="control-label col-lg-2" for="chosenPeriod">Podaj okres kredytowania </label>
					<div class="col-lg-8">
						<form:select path = "chosenPeriod" items = "${periods}"
                                                           multiple = "false" /> miesięcy

					</div>
				</div>


				<div class="form-group">

						<input type="submit" id="btnAdd" class="btn btn-primary btn-lg active" value ="Pokaż najlepsze oferty"/>
					</div>
				</div>

			</fieldset>
		</form:form>

				<h1></h1>
                			                     <p>
                            					 <a href="/webstore" class="btn btn-default">
                            						<span class="glyphicon-hand-left glyphicon"></span> Powrót
                            					 </a>
                            				     </p>

	</section>
</body>
</html>
