-- Create a new table called 'Customers' in schema 'dbo'
-- Drop the table if it already exists
IF OBJECT_ID('dbo.BankOffer', 'U') IS NOT NULL
DROP TABLE dbo.BankOffer
GO
-- Create the table in the specified schema
CREATE TABLE dbo.BankOffer
(
   BankOfferIdId        INT    NOT NULL   PRIMARY KEY, -- primary key column
   CheckDate			DATE NOT NULL,
   BankName      		[NVARCHAR](50)  NOT NULL,
   TitleOfOffer  		[NVARCHAR](50)  NOT NULL,
   ChosenCreditAmount   [NVARCHAR](50)  NOT NULL,
   ChosenPeriod  		[NVARCHAR](50)  NOT NULL,
   RRSO     			[NVARCHAR](50)  NOT NULL,
   CreditAmount     	[NVARCHAR](50)  NOT NULL,
   InstallmentAmount    [NVARCHAR](50)  NOT NULL
);
GO;

INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('1','2019-04-20','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy','1000','12','RRSO: 0%',' 1 000 zł',' 83,33 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('2','2019-04-20','PKO Bank Polski','Pożyczka got&oacute;wkowa Mini Ratka dla os&oacute;b, kt&oacute;re nie miały pożyczki/kredytu got&oacute;wkowego w PKO Banku Polskim','1000','12','RRSO: 5,11%',' 1 027,23 zł',' 85,60 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('3','2019-04-20','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna (dla klient&oacute;w nieposiadających pożyczek w banku Pekao S.A)','1000','12','RRSO: 7,22%',' 1 038,27 zł',' 86,52 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('4','2019-04-20','Citi Handlowy','Pożyczka Got&oacute;wkowa z kontem Citi Priority','1000','12','RRSO: 7,78%',' 1 041,20 zł',' 86,77 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('5','2019-04-20','PKO Bank Polski','Pożyczka got&oacute;wkowa Mini Ratka konsolidacja (zewnętrzna)','1000','12','RRSO: 9,37%',' 1 049,36 zł',' 87,45 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('6','2019-04-20','ING Bank Śląski','Pożyczka z Konsolidacją','1000','12','RRSO: 9,37%',' 1 049,36 zł',' 87,45 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('7','2019-04-20','Citi Handlowy','Kr&oacute;tkoterminowa Pożyczka Got&oacute;wkowa Online dla nowych klient&oacute;w','1000','12','RRSO: 9,38%',' 1 049,42 zł',' 87,45 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('8','2019-04-20','Cr&eacute;dit Agricole','Promocja kredytu konsolidacyjnego&quot;Konsolidacja bez prowizji&quot; ','1000','12','RRSO: 10,46%',' 1 054,93 zł',' 87,91 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('9','2019-04-20','Cr&eacute;dit Agricole','Promocja kredytu got&oacute;wkowego &quot;Got&oacute;wka bez prowizji&quot;','1000','12','RRSO: 10,46%',' 1 054,93 zł',' 87,91 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('10','2019-04-20','Cr&eacute;dit Agricole','Promocja kredytu konsolidacyjnego&quot;Konsolidacja bez prowizji z ubezpieczeniem&quot;','1000','12','RRSO: 11,57%',' 1 060,58 zł',' 88,38 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('11','2019-04-20','Cr&eacute;dit Agricole','Promocja kredytu got&oacute;wkowego &quot;Got&oacute;wka bez prowizji z ubezpieczeniem&quot; ','1000','12','RRSO: 11,57%',' 1 060,58 zł',' 88,38 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('12','2019-04-20','Millennium','Pożyczka Konsolidacyjna oferta promocyjna','1000','12','RRSO: 13,10%',' 1 068,26 zł',' 89,02 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('13','2019-04-20','Alior Bank','Pożyczka Online','1000','12','RRSO: 14,22%',' 1 073,88 zł',' 89,49 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('14','2019-04-20','Bank BPS','Bezpieczna got&oacute;wka - z ubezpieczeniem','1000','12','RRSO: 14,51%',' 1 075,34 zł',' 89,61 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('15','2019-04-20','Bank BPS','Bezpieczna got&oacute;wka - bez ubezpieczenia','1000','12','RRSO: 15,27%',' 1 079,11 zł',' 89,93 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('16','2019-04-20','PLUS BANK','Kredyt got&oacute;wkowy Tu i Teraz','1000','12','RRSO: 15,43%',' 1 079,90 zł',' 89,99 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('17','2019-04-20','Millennium','Pożyczka Konsolidacyjna','1000','12','RRSO: 15,73%',' 1 081,38 zł',' 90,12 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('18','2019-04-20','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna konsolidacyjna','1000','12','RRSO: 16,55%',' 1 085,45 zł',' 90,45 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('19','2019-04-20','Citi Handlowy','Pożyczka Got&oacute;wkowa Online dla nowych klient&oacute;w ','1000','12','RRSO: 18,44%',' 1 094,71 zł',' 91,23 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('20','2019-04-20','Millennium','Pożyczka Got&oacute;wkowa ','1000','12','RRSO: 18,53%',' 1 095,12 zł',' 91,26 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('21','2019-04-20','Santander Consumer Bank','Kredyt got&oacute;wkowy','1000','12','RRSO: 18,92%',' 1 097,03 zł',' 91,42 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('22','2019-04-20','Cr&eacute;dit Agricole','Kredyt Prostoliczony','1000','12','RRSO: 19,43%',' 1 099,51 zł',' 91,63 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('23','2019-04-20','Bank Pocztowy','Kredyt Pocztowy (z ubezpieczeniem)','1000','12','RRSO: 20,87%',' 1 106,49 zł',' 92,21 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('24','2019-04-20','mBank','Obniżamy prowizję','1000','12','RRSO: 22,07%',' 1 112,22 zł',' 92,68 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('25','2019-04-20','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna (na dowolny cel)','1000','12','RRSO: 22,07%',' 1 112,22 zł',' 92,68 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('26','2019-04-20','PKO Bank Polski','Pożyczka got&oacute;wkowa Mini Ratka &quot;standard&quot;','1000','12','RRSO: 22,07%',' 1 112,22 zł',' 92,68 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('27','2019-04-20','Bank Pocztowy','Kredyt Pocztowy','1000','12','RRSO: 22,10%',' 1 112,38 zł',' 92,70 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('28','2019-04-20','PKO Bank Polski','Pożyczka dla os&oacute;b posiadających Kartę Dużej Rodziny','1000','12','RRSO: 24,46%',' 1 123,59 zł',' 93,63 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('29','2019-04-20','ING Bank Śląski','Pożyczka Pieniężna on-line','1000','12','RRSO: 26,50%',' 1 133,21 zł',' 94,43 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('30','2019-04-20','Santander Consumer Bank','Kredyt got&oacute;wkowy (stały klient)','1000','12','RRSO: 27,79%',' 1 139,22 zł',' 94,94 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('31','2019-04-20','ING Bank Śląski','Pożyczka Pieniężna on-line (z ubezpieczeniem)','1000','12','RRSO: 29,58%',' 1 147,48 zł',' 95,62 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('32','2019-04-20','Getin Noble Bank','Kredyt got&oacute;wkowy','1000','12','RRSO: 30,77%',' 1 152,97 zł',' 96,08 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('33','2019-04-20','Citi Handlowy','Pożyczka got&oacute;wkowa','1000','12','RRSO: 31,55%',' 1 156,55 zł',' 96,38 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('34','2019-04-20','Citi Handlowy','Pożyczka Konsolidacyjna','1000','12','RRSO: 31,55%',' 1 156,55 zł',' 96,38 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('35','2019-04-20','BNP Paribas','Kredyt got&oacute;wkowy Doceniaj, Nie Oceniaj','1000','12','RRSO: 33,71%',' 1 166,33 zł',' 97,19 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('36','2019-04-20','Eurobank','Kredyt Konsolidacyjny','1000','12','RRSO: 34,14%',' 1 168,26 zł',' 97,35 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('37','2019-04-20','PLUS BANK','Kredyt Bonus Plus','1000','12','RRSO: 35,49%',' 1 174,34 zł',' 97,86 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('38','2019-04-20','BNP Paribas','iGot&oacute;wka','1000','12','RRSO: 35,80%',' 1 175,73 zł',' 97,98 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('39','2019-04-20','Eurobank','Pożyczka przez Internet','1000','12','RRSO: 36,15%',' 1 177,31 zł',' 98,11 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('40','2019-04-20','T-Mobile Usługi Bankowe','Pożyczka got&oacute;wkowa','1000','12','RRSO: 37,08%',' 1 181,42 zł',' 98,45 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('41','2019-04-20','BNP Paribas','Kredyt got&oacute;wkowy','1000','12','RRSO: 39,25%',' 1 191,02 zł',' 99,25 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('42','2019-04-20','BNP Paribas','Kredyt got&oacute;wkowy (z ubezpieczeniem)','1000','12','RRSO: 39,40%',' 1 191,69 zł',' 99,31 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('43','2019-04-20','Eurobank','Kredyt Got&oacute;wkowy','1000','12','RRSO: 40,98%',' 1 198,61 zł',' 99,88 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('44','2019-04-20','PLUS BANK','Kredyt got&oacute;wkowy (z zabezpieczeniem)','1000','12','RRSO: 61,13%',' 1 282,74 zł',' 106,89 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('45','2019-04-20','PLUS BANK','Kredyt got&oacute;wkowy','1000','12','RRSO: 66,72%',' 1 304,83 zł',' 108,74 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('46','2019-04-20','Santander Bank Polska','Kredyt got&oacute;wkowy','5000','120','RRSO: 7,59%',' 7 070,50 zł',' 58,92 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('47','2019-04-20','Nest Bank','Nest Got&oacute;wka/Konsolidacja','5000','120','RRSO: 9,24%',' 7 559,83 zł',' 63 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('48','2019-04-20','PKO Bank Polski','Pożyczka got&oacute;wkowa Mini Ratka konsolidacja (zewnętrzna)','5000','120','RRSO: 9,37%',' 7 597,30 zł',' 63,31 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('49','2019-04-20','Bank Pocztowy','Kredyt Pocztowy (z ubezpieczeniem)','5000','120','RRSO: 9,77%',' 7 716,44 zł',' 64,30 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('50','2019-04-20','PLUS BANK','Kredyt Premium Profit z ROR','5000','120','RRSO: 9,77%',' 7 716,44 zł',' 64,30 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('51','2019-04-20','PKO Bank Polski','Pożyczka dla os&oacute;b posiadających Kartę Dużej Rodziny','5000','120','RRSO: 9,93%',' 7 766,92 zł',' 64,72 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('52','2019-04-20','PLUS BANK','Kredyt konsolidacyjny Jedna Rata','5000','120','RRSO: 10,70%',' 7 996,86 zł',' 66,64 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('53','2019-04-20','Bank Pocztowy','Kredyt Pocztowy','5000','120','RRSO: 10,89%',' 8 056,58 zł',' 67,14 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('54','2019-04-20','Getin Noble Bank','Kredyt got&oacute;wkowy','5000','120','RRSO: 11,64%',' 8 285,02 zł',' 69,04 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('55','2019-04-20','Citi Handlowy','Pożyczka Konsolidacyjna','5000','120','RRSO: 12,34%',' 8 501,71 zł',' 70,85 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('56','2019-04-20','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy','5000','120','RRSO: 12,46%',' 8 540,24 zł',' 71,17 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('57','2019-04-20','Cr&eacute;dit Agricole','Kredyt Konsolidacyjny','5000','120','RRSO: 12,46%',' 8 540,24 zł',' 71,17 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('58','2019-04-20','PLUS BANK','Kredyt Bonus Plus','5000','120','RRSO: 12,50%',' 8 550,61 zł',' 71,26 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('59','2019-04-20','PLUS BANK','Kredyt konsolidacyjny','5000','120','RRSO: 13,23%',' 8 778,19 zł',' 73,15 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('60','2019-04-20','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy (z ubezpieczeniem)','5000','120','RRSO: 13,49%',' 8 859,90 zł',' 73,83 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('61','2019-04-20','T-Mobile Usługi Bankowe','Pożyczka got&oacute;wkowa','5000','120','RRSO: 13,54%',' 8 876,02 zł',' 73,97 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('62','2019-04-20','Millennium','Pożyczka Konsolidacyjna oferta promocyjna','5000','120','RRSO: 13,73%',' 8 936,83 zł',' 74,47 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('63','2019-04-20','Alior Bank','Pożyczka Online','5000','120','RRSO: 15,04%',' 9 349,22 zł',' 77,91 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('64','2019-04-20','PLUS BANK','Kredyt got&oacute;wkowy','5000','120','RRSO: 15,52%',' 9 500,68 zł',' 79,17 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('65','2019-04-20','Millennium','Pożyczka Konsolidacyjna','5000','120','RRSO: 16,75%',' 9 896,02 zł',' 82,47 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('66','2019-04-20','Santander Bank Polska','Kredyt got&oacute;wkowy','200000','120','RRSO: 7,59%',' 282 819,94 zł',' 2 356,83 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('67','2019-04-20','Nest Bank','Nest Got&oacute;wka/Konsolidacja','200000','120','RRSO: 9,24%',' 302 393,24 zł',' 2 519,94 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('68','2019-04-20','Bank Pocztowy','Kredyt Pocztowy','200000','120','RRSO: 10,89%',' 322 263,17 zł',' 2 685,53 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('69','2019-04-20','Getin Noble Bank','Kredyt got&oacute;wkowy','200000','120','RRSO: 11,64%',' 331 400,91 zł',' 2 761,67 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('70','2019-04-20','BNP Paribas','Kredyt got&oacute;wkowy','200000','120','RRSO: 12,43%',' 341 154,60 zł',' 2 842,96 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('71','2019-04-20','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy','200000','120','RRSO: 12,46%',' 341 609,55 zł',' 2 846,75 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('72','2019-04-20','Cr&eacute;dit Agricole','Kredyt Konsolidacyjny','200000','120','RRSO: 12,46%',' 341 609,55 zł',' 2 846,75 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('73','2019-04-20','T-Mobile Usługi Bankowe','Pożyczka got&oacute;wkowa','200000','120','RRSO: 13,03%',' 348 700,07 zł',' 2 905,83 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('74','2019-04-20','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy (z ubezpieczeniem)','200000','120','RRSO: 13,49%',' 354 396 zł',' 2 953,30 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('75','2019-04-20','BNP Paribas','Kredyt got&oacute;wkowy (z ubezpieczeniem)','200000','120','RRSO: 16,67%',' 394 795,66 zł',' 3 289,96 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('76','2019-04-22','Santander Bank Polska','Kredyt got&oacute;wkowy','200000','120','RRSO: 7,59%',' 282 819,94 zł',' 2 356,83 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('77','2019-04-22','Nest Bank','Nest Got&oacute;wka/Konsolidacja','200000','120','RRSO: 9,24%',' 302 393,24 zł',' 2 519,94 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('78','2019-04-22','Bank Pocztowy','Kredyt Pocztowy','200000','120','RRSO: 10,89%',' 322 263,17 zł',' 2 685,53 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('79','2019-04-22','Getin Noble Bank','Kredyt got&oacute;wkowy','200000','120','RRSO: 11,64%',' 331 400,91 zł',' 2 761,67 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('80','2019-04-22','BNP Paribas','Kredyt got&oacute;wkowy','200000','120','RRSO: 12,43%',' 341 154,60 zł',' 2 842,96 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('81','2019-04-22','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy','200000','120','RRSO: 12,46%',' 341 609,55 zł',' 2 846,75 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('82','2019-04-22','Cr&eacute;dit Agricole','Kredyt Konsolidacyjny','200000','120','RRSO: 12,46%',' 341 609,55 zł',' 2 846,75 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('83','2019-04-22','T-Mobile Usługi Bankowe','Pożyczka got&oacute;wkowa','200000','120','RRSO: 13,03%',' 348 700,07 zł',' 2 905,83 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('84','2019-04-22','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy (z ubezpieczeniem)','200000','120','RRSO: 13,49%',' 354 396 zł',' 2 953,30 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('85','2019-04-22','BNP Paribas','Kredyt got&oacute;wkowy (z ubezpieczeniem)','200000','120','RRSO: 16,67%',' 394 795,66 zł',' 3 289,96 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('86','2019-04-22','PKO Bank Polski','Pożyczka got&oacute;wkowa Mini Ratka dla os&oacute;b, kt&oacute;re nie miały pożyczki/kredytu got&oacute;wkowego w PKO Banku Polskim','5000','12','RRSO: 5,11%',' 5 136,17 zł',' 428,01 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('87','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna (dla klient&oacute;w nieposiadających pożyczek w banku Pekao S.A)','5000','12','RRSO: 7,22%',' 5 191,33 zł',' 432,61 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('88','2019-04-22','Citi Handlowy','Pożyczka Got&oacute;wkowa z kontem Citi Priority','5000','12','RRSO: 7,78%',' 5 206 zł',' 433,83 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('89','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna (na spłatę zobowiązań w innych bankach)','5000','12','RRSO: 9,21%',' 5 242,72 zł',' 436,89 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('90','2019-04-22','PKO Bank Polski','Pożyczka got&oacute;wkowa Mini Ratka konsolidacja (zewnętrzna)','5000','12','RRSO: 9,37%',' 5 246,81 zł',' 437,23 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('91','2019-04-22','ING Bank Śląski','Pożyczka z Konsolidacją','5000','12','RRSO: 9,37%',' 5 246,81 zł',' 437,23 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('92','2019-04-22','Citi Handlowy','Kr&oacute;tkoterminowa Pożyczka Got&oacute;wkowa Online dla nowych klient&oacute;w','5000','12','RRSO: 9,38%',' 5 247,09 zł',' 437,26 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('93','2019-04-22','Cr&eacute;dit Agricole','Promocja kredytu konsolidacyjnego&quot;Konsolidacja bez prowizji&quot; ','5000','12','RRSO: 10,46%',' 5 274,67 zł',' 439,56 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('94','2019-04-22','Cr&eacute;dit Agricole','Promocja kredytu got&oacute;wkowego &quot;Got&oacute;wka bez prowizji&quot;','5000','12','RRSO: 10,46%',' 5 274,67 zł',' 439,56 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('95','2019-04-22','Santander Bank Polska','Kredyt got&oacute;wkowy','5000','12','RRSO: 11,13%',' 5 291,82 zł',' 440,98 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('96','2019-04-22','Nest Bank','Nest Got&oacute;wka na Klik','5000','12','RRSO: 11,46%',' 5 300 zł',' 441,67 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('97','2019-04-22','Cr&eacute;dit Agricole','Promocja kredytu got&oacute;wkowego &quot;Got&oacute;wka bez prowizji z ubezpieczeniem&quot; ','5000','12','RRSO: 11,57%',' 5 302,91 zł',' 441,91 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('98','2019-04-22','Cr&eacute;dit Agricole','Promocja kredytu konsolidacyjnego&quot;Konsolidacja bez prowizji z ubezpieczeniem&quot;','5000','12','RRSO: 11,57%',' 5 302,91 zł',' 441,91 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('99','2019-04-22','Millennium','Pożyczka Konsolidacyjna oferta promocyjna','5000','12','RRSO: 13,10%',' 5 341,32 zł',' 445,11 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('100','2019-04-22','Alior Bank','Pożyczka Online','5000','12','RRSO: 14,22%',' 5 369,41 zł',' 447,45 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('101','2019-04-22','Bank BPS','Bezpieczna got&oacute;wka - z ubezpieczeniem','5000','12','RRSO: 14,51%',' 5 376,72 zł',' 448,06 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('102','2019-04-22','Bank BPS','Bezpieczna got&oacute;wka - bez ubezpieczenia','5000','12','RRSO: 15,27%',' 5 395,56 zł',' 449,63 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('103','2019-04-22','Millennium','Pożyczka Konsolidacyjna','5000','12','RRSO: 15,73%',' 5 406,91 zł',' 450,58 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('104','2019-04-22','Nest Bank','Nest Got&oacute;wka / Konsolidacja','5000','12','RRSO: 16,23%',' 5 419,29 zł',' 451,61 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('105','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna konsolidacyjna','5000','12','RRSO: 16,55%',' 5 427,27 zł',' 452,27 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('106','2019-04-22','Citi Handlowy','Pożyczka Got&oacute;wkowa Online dla nowych klient&oacute;w ','5000','12','RRSO: 18,44%',' 5 473,53 zł',' 456,13 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('107','2019-04-22','Millennium','Pożyczka Got&oacute;wkowa ','5000','12','RRSO: 18,53%',' 5 475,62 zł',' 456,30 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('108','2019-04-22','Santander Consumer Bank','Kredyt got&oacute;wkowy','5000','12','RRSO: 18,92%',' 5 485,13 zł',' 457,09 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('109','2019-04-22','Cr&eacute;dit Agricole','Kredyt Prostoliczony','5000','12','RRSO: 19,43%',' 5 497,56 zł',' 458,13 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('110','2019-04-22','PLUS BANK','Kredyt Premium Profit z ROR','5000','12','RRSO: 20,87%',' 5 532,46 zł',' 461,04 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('111','2019-04-22','Bank Pocztowy','Kredyt Pocztowy (z ubezpieczeniem)','5000','12','RRSO: 20,87%',' 5 532,46 zł',' 461,04 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('112','2019-04-22','mBank','Obniżamy prowizję','5000','12','RRSO: 22,07%',' 5 561,09 zł',' 463,42 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('113','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna (na dowolny cel)','5000','12','RRSO: 22,07%',' 5 561,09 zł',' 463,42 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('114','2019-04-22','PKO Bank Polski','Pożyczka got&oacute;wkowa Mini Ratka &quot;standard&quot;','5000','12','RRSO: 22,07%',' 5 561,09 zł',' 463,42 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('115','2019-04-22','Bank Pocztowy','Kredyt Pocztowy','5000','12','RRSO: 22,10%',' 5 561,91 zł',' 463,49 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('116','2019-04-22','Eurobank','Kredyt Konsolidacyjny','5000','12','RRSO: 23,77%',' 5 601,60 zł',' 466,80 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('117','2019-04-22','PKO Bank Polski','Pożyczka dla os&oacute;b posiadających Kartę Dużej Rodziny','5000','12','RRSO: 24,46%',' 5 617,97 zł',' 468,16 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('118','2019-04-22','PLUS BANK','Kredyt Profit z ROR','5000','12','RRSO: 25,26%',' 5 636,85 zł',' 469,74 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('119','2019-04-22','Santander Consumer Bank','Mistrzowski Kredyt Got&oacute;wkowy (dla stałych klient&oacute;w)','5000','12','RRSO: 25,37%',' 5 639,52 zł',' 469,96 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('120','2019-04-22','ING Bank Śląski','Pożyczka Pieniężna on-line','5000','12','RRSO: 26,50%',' 5 666,03 zł',' 472,17 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('121','2019-04-22','Eurobank','Kredyt Got&oacute;wkowy','5000','12','RRSO: 27,69%',' 5 693,71 zł',' 474,48 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('122','2019-04-22','Santander Consumer Bank','Kredyt got&oacute;wkowy (stały klient)','5000','12','RRSO: 27,79%',' 5 696,12 zł',' 474,68 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('123','2019-04-22','ING Bank Śląski','Pożyczka Pieniężna on-line (z ubezpieczeniem)','5000','12','RRSO: 29,58%',' 5 737,39 zł',' 478,12 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('124','2019-04-22','Eurobank','Pożyczka energooszczędna','5000','12','RRSO: 29,65%',' 5 739,15 zł',' 478,26 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('125','2019-04-22','Getin Noble Bank','Kredyt got&oacute;wkowy','5000','12','RRSO: 30,77%',' 5 764,85 zł',' 480,40 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('126','2019-04-22','Citi Handlowy','Pożyczka Konsolidacyjna','5000','12','RRSO: 31,55%',' 5 782,74 zł',' 481,89 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('127','2019-04-22','Citi Handlowy','Pożyczka got&oacute;wkowa','5000','12','RRSO: 31,55%',' 5 782,74 zł',' 481,89 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('128','2019-04-22','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy','5000','12','RRSO: 31,74%',' 5 787,11 zł',' 482,26 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('129','2019-04-22','Cr&eacute;dit Agricole','Kredyt Konsolidacyjny','5000','12','RRSO: 31,74%',' 5 787,11 zł',' 482,26 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('130','2019-04-22','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy (z ubezpieczeniem)','5000','12','RRSO: 32,35%',' 5 800,95 zł',' 483,41 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('131','2019-04-22','BNP Paribas','Kredyt got&oacute;wkowy Doceniaj, Nie Oceniaj','5000','12','RRSO: 33,71%',' 5 831,64 zł',' 485,97 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('132','2019-04-22','PLUS BANK','Kredyt Bonus Plus','5000','12','RRSO: 35,49%',' 5 871,72 zł',' 489,31 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('133','2019-04-22','BNP Paribas','iGot&oacute;wka','5000','12','RRSO: 35,80%',' 5 878,64 zł',' 489,89 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('134','2019-04-22','Eurobank','Pożyczka przez Internet','5000','12','RRSO: 36,15%',' 5 886,54 zł',' 490,54 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('135','2019-04-22','T-Mobile Usługi Bankowe','Pożyczka got&oacute;wkowa','5000','12','RRSO: 37,08%',' 5 907,11 zł',' 492,26 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('136','2019-04-22','BNP Paribas','Kredyt got&oacute;wkowy','5000','12','RRSO: 39,25%',' 5 955,11 zł',' 496,26 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('137','2019-04-22','BNP Paribas','Kredyt got&oacute;wkowy (z ubezpieczeniem)','5000','12','RRSO: 39,40%',' 5 958,46 zł',' 496,54 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('138','2019-04-22','PLUS BANK','Kredyt got&oacute;wkowy (z zabezpieczeniem)','5000','12','RRSO: 61,13%',' 6 413,69 zł',' 534,47 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('139','2019-04-22','PLUS BANK','Kredyt got&oacute;wkowy','5000','12','RRSO: 66,72%',' 6 524,13 zł',' 543,68 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('140','2019-04-22','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy','1000','12','RRSO: 0%',' 1 000 zł',' 83,33 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('141','2019-04-22','PKO Bank Polski','Pożyczka got&oacute;wkowa Mini Ratka dla os&oacute;b, kt&oacute;re nie miały pożyczki/kredytu got&oacute;wkowego w PKO Banku Polskim','1000','12','RRSO: 5,11%',' 1 027,23 zł',' 85,60 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('142','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna (dla klient&oacute;w nieposiadających pożyczek w banku Pekao S.A)','1000','12','RRSO: 7,22%',' 1 038,27 zł',' 86,52 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('143','2019-04-22','Citi Handlowy','Pożyczka Got&oacute;wkowa z kontem Citi Priority','1000','12','RRSO: 7,78%',' 1 041,20 zł',' 86,77 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('144','2019-04-22','PKO Bank Polski','Pożyczka got&oacute;wkowa Mini Ratka konsolidacja (zewnętrzna)','1000','12','RRSO: 9,37%',' 1 049,36 zł',' 87,45 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('145','2019-04-22','ING Bank Śląski','Pożyczka z Konsolidacją','1000','12','RRSO: 9,37%',' 1 049,36 zł',' 87,45 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('146','2019-04-22','Citi Handlowy','Kr&oacute;tkoterminowa Pożyczka Got&oacute;wkowa Online dla nowych klient&oacute;w','1000','12','RRSO: 9,38%',' 1 049,42 zł',' 87,45 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('147','2019-04-22','Cr&eacute;dit Agricole','Promocja kredytu konsolidacyjnego&quot;Konsolidacja bez prowizji&quot; ','1000','12','RRSO: 10,46%',' 1 054,93 zł',' 87,91 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('148','2019-04-22','Cr&eacute;dit Agricole','Promocja kredytu got&oacute;wkowego &quot;Got&oacute;wka bez prowizji&quot;','1000','12','RRSO: 10,46%',' 1 054,93 zł',' 87,91 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('149','2019-04-22','Cr&eacute;dit Agricole','Promocja kredytu konsolidacyjnego&quot;Konsolidacja bez prowizji z ubezpieczeniem&quot;','1000','12','RRSO: 11,57%',' 1 060,58 zł',' 88,38 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('150','2019-04-22','Cr&eacute;dit Agricole','Promocja kredytu got&oacute;wkowego &quot;Got&oacute;wka bez prowizji z ubezpieczeniem&quot; ','1000','12','RRSO: 11,57%',' 1 060,58 zł',' 88,38 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('151','2019-04-22','Millennium','Pożyczka Konsolidacyjna oferta promocyjna','1000','12','RRSO: 13,10%',' 1 068,26 zł',' 89,02 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('152','2019-04-22','Alior Bank','Pożyczka Online','1000','12','RRSO: 14,22%',' 1 073,88 zł',' 89,49 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('153','2019-04-22','Bank BPS','Bezpieczna got&oacute;wka - z ubezpieczeniem','1000','12','RRSO: 14,51%',' 1 075,34 zł',' 89,61 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('154','2019-04-22','Bank BPS','Bezpieczna got&oacute;wka - bez ubezpieczenia','1000','12','RRSO: 15,27%',' 1 079,11 zł',' 89,93 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('155','2019-04-22','PLUS BANK','Kredyt got&oacute;wkowy Tu i Teraz','1000','12','RRSO: 15,43%',' 1 079,90 zł',' 89,99 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('156','2019-04-22','Millennium','Pożyczka Konsolidacyjna','1000','12','RRSO: 15,73%',' 1 081,38 zł',' 90,12 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('157','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna konsolidacyjna','1000','12','RRSO: 16,55%',' 1 085,45 zł',' 90,45 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('158','2019-04-22','Citi Handlowy','Pożyczka Got&oacute;wkowa Online dla nowych klient&oacute;w ','1000','12','RRSO: 18,44%',' 1 094,71 zł',' 91,23 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('159','2019-04-22','Millennium','Pożyczka Got&oacute;wkowa ','1000','12','RRSO: 18,53%',' 1 095,12 zł',' 91,26 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('160','2019-04-22','Santander Consumer Bank','Kredyt got&oacute;wkowy','1000','12','RRSO: 18,92%',' 1 097,03 zł',' 91,42 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('161','2019-04-22','Cr&eacute;dit Agricole','Kredyt Prostoliczony','1000','12','RRSO: 19,43%',' 1 099,51 zł',' 91,63 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('162','2019-04-22','Bank Pocztowy','Kredyt Pocztowy (z ubezpieczeniem)','1000','12','RRSO: 20,87%',' 1 106,49 zł',' 92,21 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('163','2019-04-22','mBank','Obniżamy prowizję','1000','12','RRSO: 22,07%',' 1 112,22 zł',' 92,68 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('164','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna (na dowolny cel)','1000','12','RRSO: 22,07%',' 1 112,22 zł',' 92,68 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('165','2019-04-22','PKO Bank Polski','Pożyczka got&oacute;wkowa Mini Ratka &quot;standard&quot;','1000','12','RRSO: 22,07%',' 1 112,22 zł',' 92,68 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('166','2019-04-22','Bank Pocztowy','Kredyt Pocztowy','1000','12','RRSO: 22,10%',' 1 112,38 zł',' 92,70 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('167','2019-04-22','PKO Bank Polski','Pożyczka dla os&oacute;b posiadających Kartę Dużej Rodziny','1000','12','RRSO: 24,46%',' 1 123,59 zł',' 93,63 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('168','2019-04-22','ING Bank Śląski','Pożyczka Pieniężna on-line','1000','12','RRSO: 26,50%',' 1 133,21 zł',' 94,43 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('169','2019-04-22','Santander Consumer Bank','Kredyt got&oacute;wkowy (stały klient)','1000','12','RRSO: 27,79%',' 1 139,22 zł',' 94,94 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('170','2019-04-22','ING Bank Śląski','Pożyczka Pieniężna on-line (z ubezpieczeniem)','1000','12','RRSO: 29,58%',' 1 147,48 zł',' 95,62 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('171','2019-04-22','Getin Noble Bank','Kredyt got&oacute;wkowy','1000','12','RRSO: 30,77%',' 1 152,97 zł',' 96,08 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('172','2019-04-22','Citi Handlowy','Pożyczka got&oacute;wkowa','1000','12','RRSO: 31,55%',' 1 156,55 zł',' 96,38 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('173','2019-04-22','Citi Handlowy','Pożyczka Konsolidacyjna','1000','12','RRSO: 31,55%',' 1 156,55 zł',' 96,38 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('174','2019-04-22','BNP Paribas','Kredyt got&oacute;wkowy Doceniaj, Nie Oceniaj','1000','12','RRSO: 33,71%',' 1 166,33 zł',' 97,19 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('175','2019-04-22','Eurobank','Kredyt Konsolidacyjny','1000','12','RRSO: 34,14%',' 1 168,26 zł',' 97,35 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('176','2019-04-22','PLUS BANK','Kredyt Bonus Plus','1000','12','RRSO: 35,49%',' 1 174,34 zł',' 97,86 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('177','2019-04-22','BNP Paribas','iGot&oacute;wka','1000','12','RRSO: 35,80%',' 1 175,73 zł',' 97,98 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('178','2019-04-22','Eurobank','Pożyczka przez Internet','1000','12','RRSO: 36,15%',' 1 177,31 zł',' 98,11 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('179','2019-04-22','T-Mobile Usługi Bankowe','Pożyczka got&oacute;wkowa','1000','12','RRSO: 37,08%',' 1 181,42 zł',' 98,45 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('180','2019-04-22','BNP Paribas','Kredyt got&oacute;wkowy','1000','12','RRSO: 39,25%',' 1 191,02 zł',' 99,25 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('181','2019-04-22','BNP Paribas','Kredyt got&oacute;wkowy (z ubezpieczeniem)','1000','12','RRSO: 39,40%',' 1 191,69 zł',' 99,31 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('182','2019-04-22','Eurobank','Kredyt Got&oacute;wkowy','1000','12','RRSO: 40,98%',' 1 198,61 zł',' 99,88 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('183','2019-04-22','PLUS BANK','Kredyt got&oacute;wkowy (z zabezpieczeniem)','1000','12','RRSO: 61,13%',' 1 282,74 zł',' 106,89 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('184','2019-04-22','PLUS BANK','Kredyt got&oacute;wkowy','1000','12','RRSO: 66,72%',' 1 304,83 zł',' 108,74 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('185','2019-04-22','PKO Bank Polski','Pożyczka got&oacute;wkowa Mini Ratka dla os&oacute;b, kt&oacute;re nie miały pożyczki/kredytu got&oacute;wkowego w PKO Banku Polskim','10000','12','RRSO: 5,11%',' 10 272,35 zł',' 856,03 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('186','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna (dla klient&oacute;w nieposiadających pożyczek w banku Pekao S.A)','10000','12','RRSO: 7,22%',' 10 382,66 zł',' 865,22 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('187','2019-04-22','Citi Handlowy','Pożyczka Got&oacute;wkowa z kontem Citi Priority','10000','12','RRSO: 7,78%',' 10 412 zł',' 867,67 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('188','2019-04-22','ING Bank Śląski','Pożyczka z Konsolidacją','10000','12','RRSO: 8,83%',' 10 465,82 zł',' 872,15 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('189','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna (na spłatę zobowiązań w innych bankach)','10000','12','RRSO: 9,21%',' 10 485,44 zł',' 873,79 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('190','2019-04-22','PKO Bank Polski','Pożyczka got&oacute;wkowa Mini Ratka konsolidacja (zewnętrzna)','10000','12','RRSO: 9,37%',' 10 493,62 zł',' 874,47 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('191','2019-04-22','Citi Handlowy','Kr&oacute;tkoterminowa Pożyczka Got&oacute;wkowa Online dla nowych klient&oacute;w','10000','12','RRSO: 9,38%',' 10 494,18 zł',' 874,51 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('192','2019-04-22','Cr&eacute;dit Agricole','Promocja kredytu got&oacute;wkowego &quot;Got&oacute;wka bez prowizji&quot;','10000','12','RRSO: 10,46%',' 10 549,35 zł',' 879,11 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('193','2019-04-22','Cr&eacute;dit Agricole','Promocja kredytu konsolidacyjnego&quot;Konsolidacja bez prowizji&quot; ','10000','12','RRSO: 10,46%',' 10 549,35 zł',' 879,11 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('194','2019-04-22','Santander Bank Polska','Kredyt got&oacute;wkowy','10000','12','RRSO: 11,13%',' 10 583,63 zł',' 881,97 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('195','2019-04-22','Nest Bank','Nest Got&oacute;wka na Klik','10000','12','RRSO: 11,46%',' 10 600 zł',' 883,33 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('196','2019-04-22','Cr&eacute;dit Agricole','Promocja kredytu konsolidacyjnego&quot;Konsolidacja bez prowizji z ubezpieczeniem&quot;','10000','12','RRSO: 11,57%',' 10 605,82 zł',' 883,82 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('197','2019-04-22','Cr&eacute;dit Agricole','Promocja kredytu got&oacute;wkowego &quot;Got&oacute;wka bez prowizji z ubezpieczeniem&quot; ','10000','12','RRSO: 11,57%',' 10 605,82 zł',' 883,82 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('198','2019-04-22','Millennium','Pożyczka Konsolidacyjna oferta promocyjna','10000','12','RRSO: 13,10%',' 10 682,64 zł',' 890,22 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('199','2019-04-22','BOŚ','Przejrzysta Pożyczka','10000','12','RRSO: 14,02%',' 10 728,89 zł',' 894,07 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('200','2019-04-22','Alior Bank','Pożyczka Online','10000','12','RRSO: 14,22%',' 10 738,82 zł',' 894,90 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('201','2019-04-22','Bank BPS','Bezpieczna got&oacute;wka - z ubezpieczeniem','10000','12','RRSO: 14,51%',' 10 753,45 zł',' 896,12 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('202','2019-04-22','Bank BPS','Bezpieczna got&oacute;wka - bez ubezpieczenia','10000','12','RRSO: 15,27%',' 10 791,12 zł',' 899,26 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('203','2019-04-22','Millennium','Pożyczka Konsolidacyjna','10000','12','RRSO: 15,73%',' 10 813,83 zł',' 901,15 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('204','2019-04-22','Nest Bank','Nest Got&oacute;wka / Konsolidacja','10000','12','RRSO: 16,23%',' 10 838,58 zł',' 903,21 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('205','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna konsolidacyjna','10000','12','RRSO: 16,55%',' 10 854,54 zł',' 904,54 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('206','2019-04-22','BOŚ','EKO Pożyczka - Promocja &quot;EKOpożyczka rozkwitająca&quot;','10000','12','RRSO: 18,42%',' 10 946,03 zł',' 912,17 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('207','2019-04-22','Citi Handlowy','Pożyczka Got&oacute;wkowa Online dla nowych klient&oacute;w ','10000','12','RRSO: 18,44%',' 10 947,07 zł',' 912,26 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('208','2019-04-22','Millennium','Pożyczka Got&oacute;wkowa ','10000','12','RRSO: 18,53%',' 10 951,24 zł',' 912,60 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('209','2019-04-22','Santander Consumer Bank','Kredyt got&oacute;wkowy','10000','12','RRSO: 18,92%',' 10 970,27 zł',' 914,19 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('210','2019-04-22','Cr&eacute;dit Agricole','Kredyt Prostoliczony','10000','12','RRSO: 19,43%',' 10 995,11 zł',' 916,26 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('211','2019-04-22','Bank Pocztowy','Kredyt Pocztowy (z ubezpieczeniem)','10000','12','RRSO: 20,87%',' 11 064,93 zł',' 922,08 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('212','2019-04-22','PLUS BANK','Kredyt Premium Profit z ROR','10000','12','RRSO: 20,87%',' 11 064,93 zł',' 922,08 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('213','2019-04-22','mBank','Obniżamy prowizję','10000','12','RRSO: 22,07%',' 11 122,19 zł',' 926,85 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('214','2019-04-22','PKO Bank Polski','Pożyczka got&oacute;wkowa Mini Ratka &quot;standard&quot;','10000','12','RRSO: 22,07%',' 11 122,19 zł',' 926,85 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('215','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna (na dowolny cel)','10000','12','RRSO: 22,07%',' 11 122,19 zł',' 926,85 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('216','2019-04-22','Bank Pocztowy','Kredyt Pocztowy','10000','12','RRSO: 22,10%',' 11 123,83 zł',' 926,99 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('217','2019-04-22','Eurobank','Kredyt Konsolidacyjny','10000','12','RRSO: 23,77%',' 11 203,21 zł',' 933,60 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('218','2019-04-22','Santander Consumer Bank','Mistrzowski Kredyt Got&oacute;wkowy','10000','12','RRSO: 24,09%',' 11 218,75 zł',' 934,90 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('219','2019-04-22','PKO Bank Polski','Pożyczka dla os&oacute;b posiadających Kartę Dużej Rodziny','10000','12','RRSO: 24,46%',' 11 235,94 zł',' 936,33 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('220','2019-04-22','ING Bank Śląski','Pożyczka Pieniężna on-line','10000','12','RRSO: 24,75%',' 11 249,71 zł',' 937,48 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('221','2019-04-22','PLUS BANK','Kredyt Profit z ROR','10000','12','RRSO: 25,26%',' 11 273,70 zł',' 939,48 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('222','2019-04-22','Santander Consumer Bank','Mistrzowski Kredyt Got&oacute;wkowy (dla stałych klient&oacute;w)','10000','12','RRSO: 25,37%',' 11 279,04 zł',' 939,92 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('223','2019-04-22','Eurobank','Kredyt Got&oacute;wkowy','10000','12','RRSO: 27,69%',' 11 387,41 zł',' 948,95 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('224','2019-04-22','ING Bank Śląski','Pożyczka Pieniężna on-line (z ubezpieczeniem)','10000','12','RRSO: 27,79%',' 11 392,04 zł',' 949,34 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('225','2019-04-22','Santander Consumer Bank','Kredyt got&oacute;wkowy (stały klient)','10000','12','RRSO: 27,79%',' 11 392,24 zł',' 949,35 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('226','2019-04-22','Eurobank','Pożyczka energooszczędna','10000','12','RRSO: 29,65%',' 11 478,30 zł',' 956,52 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('227','2019-04-22','Getin Noble Bank','Kredyt got&oacute;wkowy','10000','12','RRSO: 30,77%',' 11 529,70 zł',' 960,81 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('228','2019-04-22','Citi Handlowy','Pożyczka got&oacute;wkowa','10000','12','RRSO: 31,55%',' 11 565,47 zł',' 963,79 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('229','2019-04-22','Citi Handlowy','Pożyczka Konsolidacyjna','10000','12','RRSO: 31,55%',' 11 565,47 zł',' 963,79 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('230','2019-04-22','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy','10000','12','RRSO: 31,74%',' 11 574,22 zł',' 964,52 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('231','2019-04-22','Cr&eacute;dit Agricole','Kredyt Konsolidacyjny','10000','12','RRSO: 31,74%',' 11 574,22 zł',' 964,52 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('232','2019-04-22','BOŚ','EKO Pożyczka - Promocja &quot;Pożyczka z ubezpieczeniem&quot;','10000','12','RRSO: 32,12%',' 11 591,26 zł',' 965,94 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('233','2019-04-22','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy (z ubezpieczeniem)','10000','12','RRSO: 32,35%',' 11 601,90 zł',' 966,82 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('234','2019-04-22','BNP Paribas','Kredyt got&oacute;wkowy Doceniaj, Nie Oceniaj','10000','12','RRSO: 33,71%',' 11 663,28 zł',' 971,94 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('235','2019-04-22','PLUS BANK','Kredyt Bonus Plus','10000','12','RRSO: 35,14%',' 11 727,84 zł',' 977,32 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('236','2019-04-22','BNP Paribas','iGot&oacute;wka','10000','12','RRSO: 35,80%',' 11 757,28 zł',' 979,77 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('237','2019-04-22','Eurobank','Pożyczka przez Internet','10000','12','RRSO: 36,15%',' 11 773,07 zł',' 981,09 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('238','2019-04-22','T-Mobile Usługi Bankowe','Pożyczka got&oacute;wkowa','10000','12','RRSO: 37,08%',' 11 814,22 zł',' 984,52 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('239','2019-04-22','BNP Paribas','Kredyt got&oacute;wkowy','10000','12','RRSO: 39,25%',' 11 910,21 zł',' 992,52 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('240','2019-04-22','BNP Paribas','Kredyt got&oacute;wkowy (z ubezpieczeniem)','10000','12','RRSO: 39,40%',' 11 916,92 zł',' 993,08 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('241','2019-04-22','PLUS BANK','Kredyt got&oacute;wkowy (z zabezpieczeniem)','10000','12','RRSO: 61,13%',' 12 827,38 zł',' 1 068,95 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('242','2019-04-22','PLUS BANK','Kredyt got&oacute;wkowy','10000','12','RRSO: 66,27%',' 13 030,93 zł',' 1 085,91 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('243','2019-04-22','PLUS BANK','Kredyt got&oacute;wkowy','150000','120','RRSO: 66,27%',' 13 030,93 zł',' 1 085,91 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('244','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna (na spłatę zobowiązań w innych bankach)','150000','120','RRSO: 7,45%',' 210 970,64 zł',' 1 758,09 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('245','2019-04-22','Santander Bank Polska','Kredyt got&oacute;wkowy','150000','120','RRSO: 7,59%',' 212 114,95 zł',' 1 767,62 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('246','2019-04-22','Nest Bank','Nest Got&oacute;wka/Konsolidacja','150000','120','RRSO: 9,24%',' 226 794,93 zł',' 1 889,96 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('247','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna konsolidacyjna','150000','120','RRSO: 9,27%',' 227 004,52 zł',' 1 891,70 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('248','2019-04-22','mBank','Obniżamy prowizję','150000','120','RRSO: 9,37%',' 227 918,99 zł',' 1 899,32 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('249','2019-04-22','PLUS BANK','Kredyt konsolidacyjny Jedna Rata','150000','120','RRSO: 10,70%',' 239 905,73 zł',' 1 999,21 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('250','2019-04-22','Bank Pekao','Pożyczka Ekspresowa - oferta specjalna (na dowolny cel)','150000','120','RRSO: 10,88%',' 241 571,34 zł',' 2 013,09 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('251','2019-04-22','Bank Pocztowy','Kredyt Pocztowy','150000','120','RRSO: 10,89%',' 241 697,38 zł',' 2 014,14 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('252','2019-04-22','Getin Noble Bank','Kredyt got&oacute;wkowy','150000','120','RRSO: 11,64%',' 248 550,68 zł',' 2 071,26 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('253','2019-04-22','Citi Handlowy','Pożyczka Konsolidacyjna','150000','120','RRSO: 12,34%',' 255 051,16 zł',' 2 125,43 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('254','2019-04-22','BNP Paribas','Kredyt got&oacute;wkowy','150000','120','RRSO: 12,43%',' 255 865,95 zł',' 2 132,22 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('255','2019-04-22','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy','150000','120','RRSO: 12,46%',' 256 207,16 zł',' 2 135,06 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('256','2019-04-22','Cr&eacute;dit Agricole','Kredyt Konsolidacyjny','150000','120','RRSO: 12,46%',' 256 207,16 zł',' 2 135,06 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('257','2019-04-22','PLUS BANK','Kredyt Bonus Plus','150000','120','RRSO: 12,50%',' 256 518,44 zł',' 2 137,65 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('258','2019-04-22','T-Mobile Usługi Bankowe','Pożyczka got&oacute;wkowa','150000','120','RRSO: 13,03%',' 261 525,05 zł',' 2 179,38 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('259','2019-04-22','PLUS BANK','Kredyt konsolidacyjny','150000','120','RRSO: 13,23%',' 263 345,60 zł',' 2 194,55 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('260','2019-04-22','Cr&eacute;dit Agricole','Kredyt Got&oacute;wkowy (z ubezpieczeniem)','150000','120','RRSO: 13,49%',' 265 797 zł',' 2 214,98 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('261','2019-04-22','Millennium','Pożyczka Konsolidacyjna oferta promocyjna','150000','120','RRSO: 13,73%',' 268 104,87 zł',' 2 234,21 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('262','2019-04-22','BNP Paribas','Kredyt got&oacute;wkowy (z ubezpieczeniem)','150000','120','RRSO: 16,67%',' 296 096,74 zł',' 2 467,47 zł');
INSERT INTO [dbo].[BankOffer]
           ([BankOfferIdId]
           ,[CheckDate]
           ,[BankName]
           ,[TitleOfOffer]
           ,[ChosenCreditAmount]
           ,[ChosenPeriod]
           ,[RRSO]
           ,[CreditAmount]
           ,[InstallmentAmount])
     VALUES
           ('263','2019-04-22','Millennium','Pożyczka Konsolidacyjna','150000','120','RRSO: 16,75%',' 296 880,61 zł',' 2 474,01 zł');
