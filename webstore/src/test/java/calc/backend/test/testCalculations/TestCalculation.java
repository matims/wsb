package calc.backend.test.testCalculations;

import calc.backend.test.testCalculations.datacase.PrepareIncomeData;
import com.packt.algorithms.algorithm.impl.CalculationNetIncomeImpl;
import com.packt.calc.domain.Income;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class TestCalculation {

    @Test
    public void testCalculation() {
        List<Double> list = Arrays.asList(1000d, 2000d, 3000d, 4000d, 5000d, 10000d);
        for (double l : list) {
            Income income = PrepareIncomeData.getIncomeData(l);
            Double netIncome = new CalculationNetIncomeImpl().calculateNetIncomeForEmployeeContract(income);
            Double result = 3926.1949999999997;
            System.out.println("Dochod brutto " + l + " wyliczony dochod netto " + netIncome);
        }
    }
}


