package calc.backend.test.testCalculations.datacase;

import com.packt.calc.domain.Income;

public class PrepareIncomeData {

    public PrepareIncomeData() {
        // TODO Auto-generated constructor stub
    }

    public static Income getIncomeData(Double grossIncome) {
        Income income = new Income();
        income.setGrossIncome(grossIncome);
        income.setIncomeId(1l);
        income.setTaxYear("2019");

        return income;

    }

}
