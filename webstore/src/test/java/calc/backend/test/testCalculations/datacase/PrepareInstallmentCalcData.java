package calc.backend.test.testCalculations.datacase;

import com.packt.algorithms.entities.Installment;

public class PrepareInstallmentCalcData {

    public static Installment getInstallmentCalcData() {
        Installment installment = new Installment();
        installment.setDti(0.2d);
        installment.setNetIncome(2000d);
        installment.setSocialMinimum(500d);
        return installment;
    }

}
