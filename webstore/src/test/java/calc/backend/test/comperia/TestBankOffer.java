package calc.backend.test.comperia;

import com.html.impl.BankOfferResponseHelperImpl;
import com.packt.calc.domain.BankOffer;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

public class TestBankOffer {

    @Ignore
    @Test
    public void testCalculation() throws Exception {

        List<BankOffer> bankOfferList = new BankOfferResponseHelperImpl().prepareBankOfferResponses("50000", "120");
        bankOfferList.stream().forEach(s -> System.out.println(s + " : "));
    }
}
