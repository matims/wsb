package calc.backend.test.csv;

import com.csv.impl.FillDataToCsvImpl;
import com.html.impl.BankOfferResponseHelperImpl;
import com.packt.calc.domain.BankOffer;
import org.junit.Ignore;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.html.impl.BankOfferResponseHelperImpl.removeEmptyRRSOFromList;

public class TestBankOfferCsv {

    @Ignore
    @Test
    public void test() {

        System.out.println(System.getProperty("user.dir"));

        List<BankOffer> bankOfferList = new ArrayList<>();
        List<String> amounts = Arrays.asList("1000", "2000", "3000", "4000", "5000", "6000", "7000", "8000", "9000", "10000", "20000", "30000", "40000", "50000", "60000", "70000",
                "80000", "90000", "100000", "110000", "120000", "130000", "140000", "150000", "160000", "170000", "180000", "190000", "200000");
        List<String> periods = Arrays.asList("6", "12", "18", "24", "30", "36", "42", "48", "54", "60", "66", "72", "78", "84", "90", "96", "102", "108", "114", "120");
        int probe = 0;

        for (String period : periods) {
            for (String amount : amounts) {
                try {
                    List<BankOffer> bankOffers = new BankOfferResponseHelperImpl().prepareBankOfferResponses(amount, period);
                    //TODO uzupełnij listę CSV

                    if (probe > 0) {
                        bankOffers.remove(0);
                    }
                    bankOffers = bankOffers.stream().map(this::convertBankOffer).collect(Collectors.toList());
                    bankOfferList.addAll(bankOffers);
                    removeEmptyRRSOFromList(bankOfferList);
                    System.out.println(amount + " : " + period);
                    probe++;
                } catch (Exception e) {
                    System.out.println("Błąd utworzenia pliku");
                }
            }
        }
        new FillDataToCsvImpl().fillCsv(bankOfferList);
    }

    private BankOffer convertBankOffer(BankOffer bankOffer) {
        bankOffer.setBankName(translate(bankOffer.getBankName()));
        bankOffer.setTitleOfOffer(translate(bankOffer.getTitleOfOffer()));
        bankOffer.setChosenPeriod(translate(bankOffer.getChosenPeriod()));
        bankOffer.setChosenCreditAmount(translate(bankOffer.getChosenCreditAmount()));
        bankOffer.setRRSO(translate(bankOffer.getRRSO()));
        bankOffer.setCreditAmount(translate(bankOffer.getCreditAmount()));
        bankOffer.setInstallmentAmount(translate(bankOffer.getInstallmentAmount()));

        return bankOffer;
    }

    private String translate(String word) {

        return word.replaceAll("&eacute;", "e")
                .replaceAll("ż", "z")
                .replaceAll("ę", "e")
                .replaceAll("ą", "a")
                .replaceAll("&oacute;", "o")
                .replaceAll("%", "")
                .replaceAll("zł", "")
                .replaceAll(" &quot", "");


    }


}
