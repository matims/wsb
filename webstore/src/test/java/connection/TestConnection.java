package connection;

import com.html.impl.BankOfferResponseHelperImpl;
import com.jdbc.ConnectJDBC;
import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class TestConnection {

    @Ignore
    @Test
    public void test() throws Exception {

        List<String> periods = Arrays.asList("12", "30", "36", "60");
        List<String> chosenInitialAmounts = Arrays.asList(
                "10000", "100000", "200000");

        for (String p : periods) {
            for (String i : chosenInitialAmounts) {
                List bankOfferList = new BankOfferResponseHelperImpl().prepareBankOfferResponses(i, p);
                new ConnectJDBC().insertBankOffersToDatabase(bankOfferList);
            }
        }


    }
}
